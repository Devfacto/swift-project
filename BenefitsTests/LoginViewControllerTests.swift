//
//  LoginViewControllerTests.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-20.
//  Copyright © 2016 devfacto. All rights reserved.
//

import XCTest

@testable import Benefits

class LoginViewControllerTests: XCTestCase {
    
    var sut:LoginViewController! = LoginViewController()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        sut = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LoginVC") as! LoginViewController
        
        sut.performSelectorOnMainThread(#selector(UIViewController.loadView), withObject: nil, waitUntilDone: true)
        
        sut.tableView.performSelectorOnMainThread(#selector(UITableView.reloadData), withObject: nil, waitUntilDone: true)
     
        sut.tableView(sut.tableView, cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))
        sut.tableView(sut.tableView, cellForRowAtIndexPath: NSIndexPath(forRow: 1, inSection: 0))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        
    }
    
    func testShowMainScreenAfterLoginSucceed() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    
        let expectation = expectationWithDescription("login")
        
        sut.asebpIdTextField.text = "1239959"
        sut.passwordTextField.text = "Password1"
        
        class MockAppNavigator : AppNavigator
        {
            static let sharedInsance = MockAppNavigator()
            
            var showMainScreenWasCalled: Bool = false
            
            var expectation:XCTestExpectation?
            
            override func showMainScreen(sender: LoginViewController) {
                
                expectation?.fulfill()
                
                showMainScreenWasCalled = true
            }
            
        }
        
        let mock = MockAppNavigator()
        sut.appNavigator = mock
        
        mock.expectation = expectation
        
        let button = UIButton()
        

        
        sut.onLogin(button)
        
        
        
        waitForExpectationsWithTimeout(5) { (error) in
            
           XCTAssertEqual( mock.showMainScreenWasCalled , true)
            
        }
        
    }
    
}
