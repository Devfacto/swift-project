//
//  WebApiTests.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-17.
//  Copyright © 2016 devfacto. All rights reserved.
//

import XCTest

@testable import Benefits

class WebApiTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShouldGetBearerToken() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        
        let expectation = expectationWithDescription("")
        
        WebApi.getBearerToken("1239059", password: "Password1") { (result, error) in
            if let _ = result {
                expectation.fulfill()
            }else{
                XCTFail("fail to get token")
            }
            
            
        }
        
        waitForExpectationsWithTimeout(5) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            
        }
    }
    
    func testShouldNotGetBearerTokenForInvalidCrential() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        
        let expectation = expectationWithDescription("")
        
        WebApi.getBearerToken("1239059", password: "invalid_pwd") { (result, error) in
            if let _ = error {
                expectation.fulfill()
            }else{
                XCTFail("fail to not giving token")
            }
            
            
        }
        
        waitForExpectationsWithTimeout(5) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            
        }
    }
    
        
}
