//
//  ClaimCellTests.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-15.
//  Copyright © 2016 devfacto. All rights reserved.
//

import XCTest
@testable import SwiftyJSON
@testable import Benefits


class ClaimCellViewModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShouldHideEditButtonIfEditCutOffTimePassed() {
         
        let jsonString = FileUtil.readStringFromFile("claim-338", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert( viewModel.editButtonVisibility == false)
    }
    
    
    func testShouldSetAmountLabelTextToAmountCoveredForPositivePartiallyCoveredClaim() {
        
        let jsonString = FileUtil.readStringFromFile("claim-338", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
   
        XCTAssert( viewModel.amountLabelText == "$150.00")
        XCTAssert( viewModel.amountNotCoveredLabelText == "$238.88")
    }
    
    func testShouldSetAmountLabelColorForPositive() {
        
        let jsonString = FileUtil.readStringFromFile("claim-338", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert( viewModel.amountLabelTextColor == UIColor.statusColorForPositive())
    }
    
    
    func testShouldSetStatusLabelColorForPositive() {
        
        let jsonString = FileUtil.readStringFromFile("claim-338", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert( viewModel.amountLabelTextColor == UIColor.statusColorForPositive())
    }
    
    func testShouldSetAmountLabelTextToAmountNotCoveredForNegativePartiallyCoveredClaim() {
        
        let jsonString = FileUtil.readStringFromFile("claim-340", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        print(viewModel.amountLabelText)
        XCTAssert( viewModel.amountLabelText == "$238.88")
    }
    
    
    func testShouldSetAmountLabelColorForNegative() {
        
        let jsonString = FileUtil.readStringFromFile("claim-339", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert( viewModel.amountLabelTextColor == UIColor.statusColorForNegative())
    }
    
    
    func testShouldSetStatusLabelColorForNegative() {
        
        let jsonString = FileUtil.readStringFromFile("claim-339", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert( viewModel.amountLabelTextColor == UIColor.statusColorForNegative() )
    }
   
    func testShouldHideEditButtonIfCutOffTimeNotPassed() {
        
        let jsonString = FileUtil.readStringFromFile("claim-338", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert( viewModel.editButtonVisibility == false )
    }
    
    func testShouldShowEditButtonIfCutOffTimeNotPassed() {
        
        let jsonString = FileUtil.readStringFromFile("claim-339", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert( viewModel.editButtonVisibility == true )
    }
    
    func testShouldHideNotCoveredLabelForFullyCoveredClaim() {
        let jsonString = FileUtil.readStringFromFile("claim-339", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        
        XCTAssert( viewModel.amountNotCoveredLabelVisibility == false )
        XCTAssert( viewModel.amountNotCoveredstatusLabelVisibility == false )

    }
    
    func testShouldHideNotCoveredLabelForPartialyCoveredClaim() {
        let jsonString = FileUtil.readStringFromFile("claim-338", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        
        XCTAssert( viewModel.amountNotCoveredLabelVisibility == true )
        XCTAssert( viewModel.amountNotCoveredstatusLabelVisibility == true )
        XCTAssert( viewModel.amountNotCoveredStatusLabelText == "remaining" )
        
    }
    
    func testShouldSetPostiveResultForNestedHSAExpense() {
        let jsonString = FileUtil.readStringFromFile("claim-2239", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        
        XCTAssert( viewModel.expensePaidStatusLabelTextColor == UIColor.statusColorForPositive() )
        XCTAssert( viewModel.expensePaidAmountLabelTextColor == UIColor.statusColorForPositive() )
        XCTAssert( viewModel.expenseRemainsAmountLabelTextColor == UIColor.statusColorForPositive() )
        XCTAssert( viewModel.expenseRemainsStatusLabelTextColor == UIColor.statusColorForPositive() )
        XCTAssert( viewModel.expenseAmountLabelTextColor == UIColor.statusColorForPositive() )
        XCTAssert( viewModel.expenseStatusLabelTextColor == UIColor.statusColorForPositive() )
        
        XCTAssert(viewModel.expenseAmountLabelText == "$780.00")
        XCTAssert(viewModel.expenseStatusLabelText == "eligible")
        XCTAssert(viewModel.expensePaidAmountLabelText == "$700.00")
        
        
        XCTAssert(viewModel.expenseEditButtonVisiblity == false)
    }
    
    
    func testShouldSetNegativeResultForNestedHSAExpense() {
        let jsonString = FileUtil.readStringFromFile("claim-2240", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        
        XCTAssert( viewModel.expensePaidStatusLabelTextColor == UIColor.statusColorForNegative() )
        XCTAssert( viewModel.expensePaidAmountLabelTextColor == UIColor.statusColorForNegative() )
        XCTAssert( viewModel.expenseRemainsAmountLabelTextColor == UIColor.statusColorForNegative() )
        XCTAssert( viewModel.expenseRemainsStatusLabelTextColor == UIColor.statusColorForNegative() )
        XCTAssert( viewModel.expenseAmountLabelTextColor == UIColor.statusColorForNegative() )
        XCTAssert( viewModel.expenseStatusLabelTextColor == UIColor.statusColorForNegative() )
  
    }
    
    func testShouldShowEditExpenseButtonForSubmittedHSAEditCutOffTimeInTheFuture() {
        let jsonString = FileUtil.readStringFromFile("claim-2241", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert(viewModel.expenseEditButtonVisiblity == true)
        
    }
    
    
    func testShouldHideEditExpenseButtonForSubmittedHSAEditCutOffTimeInThePast() {
        let jsonString = FileUtil.readStringFromFile("claim-2240", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert(viewModel.expenseEditButtonVisiblity == false)
        
    }
    
    
    func testShouldHideHSAPaidLabelsForFullyPaidHSAExpense() {
        let jsonString = FileUtil.readStringFromFile("claim-2241", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert(viewModel.expensePaidStatusLabelVisibility == false)
        XCTAssert(viewModel.expensePaidAmountLabelVisibility == false)
        
    }
    
    
    func testShouldHideHSAPaidLabelsForZeroAmountPaidHSAExpense() {
        let jsonString = FileUtil.readStringFromFile("claim-2240", ext: "json")!
        let claim = Claim(json: JSON.parse(jsonString))
        
        let viewModel = ClaimCellViewModel(claim: claim)
        XCTAssert(viewModel.expensePaidStatusLabelVisibility == false)
        XCTAssert(viewModel.expensePaidAmountLabelVisibility == false)
        
    }
}
