# ASEBP iOS Mobile App 1.0.0


### Congratulations! You've completed the grand tour of the CODE hub!


# Third Party Libraries in the project

* [Alamofire](https://github.com/Alamofire/Alamofire)

* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON)

* [IDMPhotoBrowser](https://github.com/ideaismobile/IDMPhotoBrowser)

* [SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD)

* [FLAnimatedImage](https://github.com/Flipboard/FLAnimatedImage)

* [CCBottomRefreshControl](https://github.com/vlasov/CCBottomRefreshControl)

# How to Run Code Coverage using [Slather](https://github.com/SlatherOrg/slather)

Install Slather on local Mac workstation. 
CD code src root dir.
RUN:
    slather coverage -s --scheme MyASEBP --workspace MyASEBP-iOS.xcworkspace MyASEBP-iOS.xcodeproj

## Create a pull request to contribute your changes back into master
Pull requests are the way to move changes from a topic branch back into the master branch.

Click on the **Pull Requests** page in the **CODE** hub, then click "New Pull Request" to create a new pull request from your topic branch to the master branch.

When you are done adding details, click "Create Pull request". Once a pull request is sent, reviewers can see your changes, recommend modifications, or even push follow-up commits.

First time creating a pull request?  [Learn more](http://go.microsoft.com/fwlink/?LinkId=533211&clcid=0x409)
# Next steps

If you haven't done so yet:
* [Install Git](http://git-scm.com/downloads)

Then clone this repo to your local machine to get started with your own project.

Happy coding!
