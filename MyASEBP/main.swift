//
//  main.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-16.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

 

let isRunningTests = NSClassFromString("XCTestCase") != nil
let appDelegateClass : AnyClass = isRunningTests ? TestingAppDelegate.self : AppDelegate.self
UIApplicationMain(Process.argc, Process.unsafeArgv, nil, NSStringFromClass(appDelegateClass))
