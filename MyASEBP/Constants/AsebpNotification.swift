//
//  AsebpNotification.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-03.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

struct AsebpNotification {
    static let SelectedMembershipSwitchedNotifiction = "SelectedMembershipSwitchedNotifiction"
    static let MembershipDetailInfoLoadedNotifiction = "MembershipDetailInfoLoadedNotifiction"

}
