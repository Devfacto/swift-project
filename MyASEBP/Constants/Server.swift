//
//  Server.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-18.
//  Copyright © 2016 devfacto. All rights reserved.
//
import Foundation

struct Server {
    
    // DEV
    //static let BaseURL = "http://104.219.156.13/MyASEBP.MobileApiTEST/api/v100"
    //"http://api.asebp.ab.ca/MyASEBP.MobileApiTEST/api/v100"
    static var BaseURL: String {
        get {
            //If this text changes, also change in Default val in Root.plist in Settings bundle
            
            return NSUserDefaults.standardUserDefaults().stringForKey("ws_env") ?? "http://env01/MyASEBP.MobileApi/api/v100"

        }
    }
    
    static let BearerTokenDispatcherURL = "\(BaseURL)/account/login"
}
