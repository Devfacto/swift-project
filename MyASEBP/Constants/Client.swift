//
//  Client.swift
//  MyASEBP-iOS
//
//  Created by Kevin Fan on 2016-05-27.
//  Copyright © 2016 devfacto. All rights reserved.
//

struct Client {
    static let ContactPhoneNumber = "1-877-431-4786"
    static let ContactEmail = "benefits@asebp.ab.ca"
    static let ContactMapUrl = "http://maps.apple.com/?address="
    static let ContactAddress = "Suite 700 Weber Centre \n" +
                                "5555 Calgary Trail \n" +
                                "Edmonton AB T6H 5P9"
    
    static let HoursOperation = "8:00 a.m. – 4:30 p.m. \n" +
                                "Monday – Friday"
}