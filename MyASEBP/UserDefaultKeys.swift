//
//  UserDefaultKeys.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-17.
//  Copyright © 2016 devfacto. All rights reserved.
//

struct UserDefaultKeys {
    static let CurrentUserJSON = "CurrentUserJSON"
    static let SelectedMembershipId = "SelectedMembershipId"
    //CurrentUserJSON saving full member info locally. loading will be slower.
    // this is to quick display previously logged in user id on login screen.
    static let CurrentUserAsebpIdOrEmail = "CurrentUserAsebpIdOrEmail"
    static let CachedClaimListJson = "CachedClaimListJson"
}
