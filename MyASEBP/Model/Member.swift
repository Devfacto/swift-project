//
//  Member.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-18.
//  Copyright © 2016 devfacto. All rights reserved.
//
import SwiftyJSON

public protocol JSONConstructable {
    init(json:JSON)
}

final class Member : JSONConstructable
{
    var _json:JSON
    
    var id:String?
    var firstName:String?
    var fullName:String?
    var lastName:String?
    var issuedDate:NSDate?
    
    init(json:JSON){
        _json = json
        
        id =  String(self._json["asebpId"])
        firstName = String(self._json["firstName"])
        fullName = String(self._json["fullName"])
        lastName = String(self._json["lastName"])
        issuedDate =  self._json["idIssueDate"].dateTime
    }
     
    
    lazy var dependants:[Dependant]? = {
        
        
        
        if let list = self._json["dependants"].array {
            let objects: [Dependant] = list.map({return Dependant(json: $0)})//.sort({$0.name < $1.name})
            
            return objects
        }
        
        // TODO: convert to pretty map style
        //         return self._json["Benefits"].array.map{
        //            (benefitJson) -> Benefit in
        //
        //            return Benefit( json:JSON(benefitJson) )
        //        }
        
        return []
    }()

    lazy var groupCode:String? = {
        
        return self._json["groupCode"].string ?? "19930"
    
    }()
    
    lazy var memberships:[Membership]? = {
        
        
        
        if let memberships = self._json["memberships"].array {
            let objects: [Membership] = memberships.map({return Membership(json: $0)})//.sort({$0.name < $1.name})
            
            return objects
        }
        
        // TODO: convert to pretty map style
        //         return self._json["Benefits"].array.map{
        //            (benefitJson) -> Benefit in
        //
        //            return Benefit( json:JSON(benefitJson) )
        //        }
        
        return []
    }()

    
    static var currentUserAsebpIdOrEmail:String? {
    
        get{
            if let id = NSUserDefaults.standardUserDefaults().objectForKey(UserDefaultKeys.CurrentUserAsebpIdOrEmail) as? String
            {
            return id
            
            }
            
            return nil
        }
        
        set{
        
            if let newValue = newValue {
                NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: UserDefaultKeys.CurrentUserAsebpIdOrEmail)
            
            }else{
            
                NSUserDefaults.standardUserDefaults().removeObjectForKey(UserDefaultKeys.CurrentUserAsebpIdOrEmail)
            }
            
            NSUserDefaults.standardUserDefaults().synchronize()
        
        }
    }
    
    static var currentMember:Member? {
        
        get{
            if let json = NSUserDefaults.standardUserDefaults().objectForKey(UserDefaultKeys.CurrentUserJSON) as? String
            {
                let member = Member(json: JSON.parse(json))
              
                
                return member
                
            }
            
            return nil
        }
        
        set{
            
            if let newValue = newValue {
                
                if Member.selectedMembershipId == nil
                {
                    if let memberships = newValue.memberships
                    {
                        Member.selectedMembershipId = memberships.first!.id
                    }
                }

                let dict =  newValue._json.rawString()
                
                
                NSUserDefaults.standardUserDefaults().setObject(dict, forKey: UserDefaultKeys.CurrentUserJSON)
                
                
            }else{
                
                NSUserDefaults.standardUserDefaults().removeObjectForKey(UserDefaultKeys.CurrentUserJSON)
            }
            
            NSUserDefaults.standardUserDefaults().synchronize()
            
        }
    }
    
    static var selectedMembership:Membership? {
        get {
            if let memberships = Member.currentMember?.memberships
            {
                 let membership = memberships.filter({ (membership) -> Bool in
                    membership.id == Member.selectedMembershipId
                })
                
                if membership.count > 0 {
                    return membership.first!
                }
                
                
            }
            
            return Member.currentMember?.memberships!.first
        }
        
        
        
    }
    
    static var selectedMembershipId:String? {
        
        get{
            if let selectedMembershipId = NSUserDefaults.standardUserDefaults().objectForKey(UserDefaultKeys.SelectedMembershipId) as? String
            {
                return selectedMembershipId
                
            }
            
            return nil
        }
        
        set{
            
            if let newValue = newValue {
                NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: UserDefaultKeys.SelectedMembershipId)
                
            }else{
                
                NSUserDefaults.standardUserDefaults().removeObjectForKey(UserDefaultKeys.SelectedMembershipId)
            }
            
            NSUserDefaults.standardUserDefaults().synchronize()
            
        }
    }


}

final class Membership
{
    
    var _json:JSON
    
    var id:String?
    var schoolJurisdiction:String?
    var schoolJurisdictionGroup:String?
    var allowHsa:Bool
    var allowWsa:Bool

    
    init(json:JSON){
        _json = json
        
        id =  String(self._json["memberId"])
        schoolJurisdiction = String(self._json["schoolJurisdiction"])
        schoolJurisdictionGroup = String(self._json["schoolJurisdictionGroup"])
        allowHsa = self._json["allowsHsa"].boolValue
        allowWsa = self._json["allowsWsa"].boolValue
    }
    

    lazy var hasEHC:Bool = {
        return self.benefits?.filter({ (benefit) -> Bool in
            
            benefit.name == "Extended Health Care"
        }).count > 0
        
    

    }()
    
    lazy var hasVision:Bool = {
        return self.benefits?.filter({ (benefit) -> Bool in
            
            benefit.name == "Vision Care"
        }).count > 0
        
        
        
    }()

 
    lazy var sectionCode:String? = {
        return self._json["sectionCode"].string ?? "NotFound"
    }()
 
    
    lazy var benefits:[Benefit]? = {
        
        
        
        if let benefits = self._json["benefits"].array {
            let benefitObjects: [Benefit] = benefits.map({return Benefit(json: $0)})//.sort({$0.name < $1.name})
            
            return benefitObjects
        }
        
        return []
    }()
    
    
    lazy var coveragePeriods:[CoveragePeriod] = {
        
        if let list = self._json["coveragePeriods"].array {
            let objects: [CoveragePeriod] = list.map({return CoveragePeriod(json: $0)})//.sort({$0.name < $1.name})
            
            return objects
        }
        
        return []
        
    }()

    
}

class CoveragePeriod
{
    private var _json:JSON
    
    var allocationSet:Bool
    var lastActiveDate:NSDate
    var firstActiveDate:NSDate

    var status:String
    
    
    init(json:JSON)
    {
        _json = json
        
        allocationSet = Bool(self._json["allocationSet"].numberValue)
        lastActiveDate = self._json["lastActiveDate"].dateTime!
        firstActiveDate = self._json["firstActiveDate"].dateTime!
 
        status = self._json["currentStatus"].stringValue
        
    }
    
    lazy var hsa :SpendingAccount? = {
        if self._json["hsa"] != nil
        {
            return SpendingAccount(json: self._json["hsa"])
        }
        return nil
    
    }()
    lazy var wsa :SpendingAccount? = {
    
        if self._json["wsa"] != nil
        {
            return SpendingAccount(json: self._json["wsa"])
        }
        return nil
    
    }()


}

class SpendingAccount
{
    var accountType:String
    var allocationPercentage:Float
    var currentBalance:NSDecimalNumber
 
    var totalDepositedAmount:NSDecimalNumber
    
    private var _json:JSON
    
    init(json:JSON)
    {
        _json = json
        
        accountType = self._json["accountType"].stringValue
        allocationPercentage = self._json["allocationPercentage"].floatValue
        totalDepositedAmount = NSDecimalNumber(double:self._json["totalDepositedAmount"].doubleValue)
        currentBalance = NSDecimalNumber(double:self._json["currentBalance"].doubleValue)
 

        
    }
}

class Dependant
{
    private var _json:JSON
    var id:String?
    
    init(json:JSON)
    {
        _json = json
        
        id = String(self._json["asebpId"])
    }
    
//    lazy var id:String? = {return String(self._json["asebpId"])}()
    lazy var firstName:String? = {return String(self._json["firstName"])}()
    lazy var fullName:String? = {return String(self._json["fullName"])}()
    lazy var lastName:String? = {return String(self._json["lastName"])}()
    
}



class ExpenseCategory
{
    private var _json:JSON
    var id:String
    var name:String
    var categoryName:String
    
    init(json:JSON)
    {
        _json = json
        
        id = self._json["id"].stringValue
        categoryName = self._json["categoryName"].stringValue
        name = self._json["name"].stringValue
        
    }

    
}
