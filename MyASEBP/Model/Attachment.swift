//
//  Attachment.swift
//  MyASEBP-iOS
//
//  Created by Stephen Visser on 2016-07-21.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import SwiftyJSON

enum AttachmentType: String {
    
    case EOB = "ExplanationOfBenefits"
    case Receipt = "Receipt"
    case Other = "Other"
    
    var singularDetail: String {
        get {
            switch self {
            case .EOB: return "Explanation of Benefits"
            case .Receipt: return "Receipt"
                
            default: return "Other Attachment"
            }
            
        }
    }
}

enum FileType : String {
    
    case Image = "Image"
    case Pdf = "Pdf"
    
}

struct Attachment
{
    
    let id: String?
    let fileName: String?
    let type: AttachmentType
    let fileType: FileType
    
    init(json: JSON)
    {
        
        id = json["id"].stringValue
        fileName = json["fileName"].string
        type = AttachmentType(rawValue: json["attachmentCategory"].stringValue)!
        fileType = FileType(rawValue: json["fileType"].stringValue) ?? .Image
        
    }
    
    //User created
    init(type: AttachmentType, fileType: FileType) {
        self.type = type
        self.fileType = fileType
        fileName = nil
        id = nil
    }

}