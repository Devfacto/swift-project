//
//  Contact.swift
//  MyASEBP-iOS
//
//  Created by Kevin Fan on 2016-07-22.
//  Copyright © 2016 devfacto. All rights reserved.
//

import SwiftyJSON


struct Contact{
    let contactPhoneNumber: String?
    let contactEmail: String?
    let contactAddress: String?
    let hoursOperation: String?
    
    init(json: JSON)
    {
        contactPhoneNumber = json["phone"].string
        contactEmail = json["email"].string
        contactAddress = json["contactAddress"].string
        hoursOperation = json["hoursOperation"].string
    }

}