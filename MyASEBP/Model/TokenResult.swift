//
//  TokenResult.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-22.
//  Copyright © 2016 devfacto. All rights reserved.
//

import SwiftyJSON

class TokenResult : JSONConstructable{
    
    var _json:JSON
    
    var accessToken:String
    var username:String
    
    
    required init(json:JSON){
        _json = json
        
        accessToken = _json["access_token"].stringValue
        username = _json["username"].stringValue
        
    }
    
    var asebpId:String{
        get{
            return username.componentsSeparatedByString("|").last!
        }
    }

}
