//
//  Claim.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-31.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import SwiftyJSON

enum ClaimType {
    case HSA
    case WSA
    case CoreClaim(OnlineClaimType)
}

enum OnlineClaimType: String {
    case ExtendedHealthCare = "ExtendedHealthCare"
    case ExtendedHealthCare2 = "Extended Health Care"
    case Vision = "Vision Care"
    case Dental = "Dental Care"
}

enum ClaimState {
    case Editable
    case NotEditable
    case HasNotYetProcessedExpense
    case HasProcessedExpense
    case CandidateForExpense
}

enum ClaimStatus {
    case Processed(positivity: StatusPositivity, description: String)
    case Submitted(positivity: StatusPositivity, description: String)
    
    var description: String {
        switch self {
        case let .Processed(_, description):return description
        case let .Submitted(_, description):return description
        }
    }
    
    var positivity: StatusPositivity {
        switch self {
        case let .Processed(positivity, _):return positivity
        case let .Submitted(positivity, _):return positivity
        }
    }
}

enum StatusPositivity: String {
    case Normal = "Neutral"
    case Positive = "Positive"
    case Negative = "Negative"
}

class Claim {

    var id: String?
    var type: ClaimType
    var serviceDescription: String?
    var recipientName: String?
    var recipientAsebpId:String?
    var providerName: String?
    var amount: NSDecimalNumber?
    var cobAmount: NSDecimalNumber?
    var amountCovered: NSDecimalNumber?
    var serviceDate: NSDate?
    var submittedDate: NSDate?
    var asebpId: String?
    var memberId: String?
    var status: ClaimStatus?
    var editDeleteCutoffTimeUtc: NSDate?
    var groupCode: String?
    var expense: Claim?
    var attachments: [Attachment]

    var amountNotCovered: NSDecimalNumber? {
        guard let a = amount, ac = amountCovered else { return nil }

        return a.decimalNumberBySubtracting(ac)
    }
    
    var state: ClaimState {
        
        if let editCutOffTime = editDeleteCutoffTimeUtc where editCutOffTime.laterDate(NSDate()) == editCutOffTime
        {
            return .Editable
        }
        
        switch type {
        case .CoreClaim:
            if let expense = expense
            {
                if case .Submitted = expense.status! {
                    return .HasNotYetProcessedExpense
                }
                
                return .HasProcessedExpense
                
            } else if case .Processed = status! where amount!.compare(amountCovered!.decimalNumberBySubtracting(cobAmount!)) == NSComparisonResult.OrderedDescending {
                return .CandidateForExpense
            }
            fallthrough
        default: return .NotEditable
        }
    }
    
    init(json:JSON) {
        
        id = json["id"].stringValue
        
        switch json["type"].stringValue {
        case "HSA": type = .HSA
        case "WSA": type = .WSA
        case "OnlineClaim": type = .CoreClaim(OnlineClaimType(rawValue: json["onlineClaimType"].stringValue)!)
        default: fatalError("Problem parsing the claim type")
        }
        
        serviceDescription = json["description"].stringValue
        recipientName = json["recipientName"].stringValue
        providerName = json["providerName"].stringValue
        amount = NSDecimalNumber(double:json["amount"].doubleValue)
        cobAmount = NSDecimalNumber(double:json["cobAmount"].doubleValue)
        amountCovered = NSDecimalNumber(double:json["amountCovered"].doubleValue)
        serviceDate = json["serviceDate"].dateTime!
        submittedDate = json["submittedDate"].dateTime!
        asebpId = json["asebpId"].stringValue
        memberId = json["memberId"].stringValue
        
        let statusJSON = json["status"]
        status = statusJSON["status"].string.map {
            switch $0 {
            case "Processed":  return .Processed(positivity: StatusPositivity(rawValue: statusJSON["outcome"].stringValue)!, description: statusJSON["statusDescription"].stringValue)
            case "Submitted": return .Submitted(positivity: StatusPositivity(rawValue: statusJSON["outcome"].stringValue)!, description: statusJSON["statusDescription"].stringValue)
            default: fatalError("Problem parsing status")
            }
        }
        
        editDeleteCutoffTimeUtc = json["editDeleteCutoffTimeUtc"].dateTime
        groupCode = json["groupCode"].stringValue
        
        let expenseJSON = json["expense"]
        if !expenseJSON.isEmpty {
            expense = Claim(json: expenseJSON)
        }
        attachments = (json["attachments"].array ?? []).map(Attachment.init)
        
    }
    
    //TODO: Once HSA is merged w/ claim style, we won't need to create new Claims
    init(forNewClaimWithType type:ClaimType)
    {
        
        self.type = type
        self.attachments = []
    }

}