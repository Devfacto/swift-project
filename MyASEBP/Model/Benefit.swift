//
//  Benefit.swift
//  MyASEBP-iOS
//
//  Created by Kevin Fan on 2016-06-14.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import SwiftyJSON

class Benefit
{
    private var _json:JSON
    
    init(json:JSON)
    {
        _json = json
    }
    
    lazy var coverageLevel:String? = {
        if self._json["coverage"] == nil
        {
            return "Not Set"
        }
        return String(self._json["coverage"])
    }()
    lazy var name:String? = {return String(self._json["name"])}()
    
}