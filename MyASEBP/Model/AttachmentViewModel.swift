//
//  AttachmentViewModel.swift
//  MyASEBP-iOS
//
//  Created by Stephen Visser on 2016-07-21.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

protocol AttachmentViewModel {
    var id: String? { get }
}

class ImageAttachmentViewModel: AttachmentViewModel {
    var id: String?
    var image: UIImage?
    
    //User added
    init(image: UIImage) {
        id = nil
        self.image = image
    }
    
    //From server
    init(attachment: Attachment) {
        id = attachment.id
        image = nil
    }
}

class PDFAttachmentViewModel: AttachmentViewModel {
    let id: String?
    var pdf: NSData?
    
    //From server
    init(attachment: Attachment) {
        id = attachment.id
        pdf = nil
    }
    
}