//
//  SingleDatePickerCellTableViewCell.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-10.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class SingleDatePickerCell: UITableViewCell {

    @IBOutlet weak var datePicker: UIDatePicker!

}
