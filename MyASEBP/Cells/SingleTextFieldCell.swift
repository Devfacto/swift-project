//
//  SingleTextFieldCell.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-17.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class SingleTextFieldCell: UITableViewCell, Validatable {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var fieldImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textField.attributedPlaceholder = NSAttributedString(string: "How Much", attributes: [NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
    }
    
    var validationState: ValidationStatus? {
        willSet {
            if let s = validationState {
                switch s {
                case .Empty, .Valid:
                    
                    fieldImage.highlighted = false
                    textField.textColor = UIColor.blackColor()
                    
                case .Error:
                    
                    fieldImage.highlighted = true
                    textField.textColor = UIColor(red: 0.984, green: 0.388, blue: 0.435, alpha: 1)
                    
                }
            }
        }
    }
    
    var disabled: Bool? {
        willSet {
            textField.enabled = !(newValue ?? false )
        }
    }

}
