//
//  ClaimCell.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-01.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class ClaimCell: UITableViewCell {

    @IBOutlet weak var constraintSubmitToHSAButtonHeight: NSLayoutConstraint?
    @IBOutlet weak var constraintEditButtonHeight: NSLayoutConstraint?
    @IBOutlet weak var expenseEditButtonHeight: NSLayoutConstraint?
    @IBOutlet weak var constraintAmountNotCoveredStatusLabelHeight: NSLayoutConstraint?
    @IBOutlet weak var constraintAmountNotCoveredLabelHeight: NSLayoutConstraint?
    @IBOutlet weak var constraintExpensePaidAmountLabelHeight: NSLayoutConstraint?
    @IBOutlet weak var constraintExpensePaidStatusLabelHeight: NSLayoutConstraint?
    @IBOutlet weak var constraintSubmitToHsaButtonWidth: NSLayoutConstraint?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var amountLabel: UILabel?
    @IBOutlet weak var amountNotCoveredLabel: UILabel?
    @IBOutlet weak var amountNotCoveredStatusLabel: UILabel?
    @IBOutlet weak var amountCoveredLabel: UILabel?
    @IBOutlet weak var amountExpensedLabel: UILabel?
    @IBOutlet weak var expenseAmountLabel: UILabel?
    @IBOutlet weak var expenseStatusLabel: UILabel?
    @IBOutlet weak var expensePaidAmountLabel: UILabel?
    @IBOutlet weak var expensePaidStatusLabel: UILabel?

    @IBOutlet weak var expenseRemainsAmountLabel: UILabel?
    @IBOutlet weak var expenseRemainsStatusLabel: UILabel?

    @IBOutlet weak var statusLabel: UILabel?
    @IBOutlet weak var serviceDateLabel: UILabel?
    @IBOutlet weak var submitDateLabel: UILabel?
    @IBOutlet weak var recipientNameLabel: UILabel?
    @IBOutlet weak var editButton: UIButton?
    @IBOutlet weak var deleteButton: UIButton?
    @IBOutlet weak var reviewImageButton: UIButton?
    @IBOutlet weak var submitToHsaButton: UIButton?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.submitToHsaButton?.titleLabel!.lineBreakMode = .ByWordWrapping
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    var claim:Claim! {
        didSet  {
            
            self.reviewImageButton?.hidden = true
            self.editButton?.hidden = true
            
          
            
            if let claim = claim
            {
                self.recipientNameLabel?.text = claim.recipientName
 
                let prefix: String
                switch claim.type {
                case .CoreClaim: prefix = "Core Claim:"
                case .HSA: prefix = "HSA:"
                case .WSA: prefix = "WSA:"
                }
    
                var description = claim.serviceDescription!
                #if DEBUG
                    description += " - (\(claim.id!))"
                #endif
                let infoString =  "\(prefix)\(description)"
                let attString = NSMutableAttributedString(string: infoString)
                
                attString.addAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size:17.0)!], range: NSMakeRange(0, prefix.characters.count - 1))
                attString.addAttributes([NSFontAttributeName: UIFont(name: "HelveticaNeue", size:17.0)!], range: NSMakeRange(prefix.characters.count, description.characters.count - 1))
                
                
                self.descriptionLabel?.attributedText = attString
                
                self.submitDateLabel?.text = (claim.submittedDate?.shortDateFormat()).map { "Submitted on \($0)" }
                self.serviceDateLabel?.text = (claim.serviceDate?.shortDateFormat()).map { "Service on \($0)" }
                
                let viewModel = ClaimCellViewModel(claim: claim)
                
                self.amountLabel?.textColor = viewModel.amountLabelTextColor
                self.statusLabel?.textColor = viewModel.statusLabelTextColor
                self.amountLabel?.text = viewModel.amountLabelText
                self.statusLabel?.text = viewModel.statusLabelText
                self.editButton?.hidden = !viewModel.editButtonVisibility
                
                if self.editButton?.hidden == true {
                    constraintEditButtonHeight?.constant = 0
                    constraintEditButtonHeight?.active = true
//                    self.setNeedsLayout()
       
                }else{
 

                    constraintEditButtonHeight?.active = false
                }
                

                self.amountNotCoveredLabel?.text = viewModel.amountNotCoveredLabelText
                self.amountNotCoveredStatusLabel?.text = viewModel.amountNotCoveredStatusLabelText
                self.amountNotCoveredLabel?.hidden = !viewModel.amountNotCoveredLabelVisibility
                self.amountNotCoveredStatusLabel?.hidden = !viewModel.amountNotCoveredstatusLabelVisibility
                
                if !viewModel.amountNotCoveredLabelVisibility {
                    constraintAmountNotCoveredStatusLabelHeight?.constant = 0
                    constraintAmountNotCoveredLabelHeight?.constant = 0
                }
                
                self.amountExpensedLabel?.text = viewModel.amountExpensedLabelText
                self.expenseStatusLabel?.text = viewModel.expenseStatusLabelText
                self.expenseAmountLabel?.text = viewModel.expenseAmountLabelText
                self.expenseRemainsAmountLabel?.text = viewModel.expensePaidAmountLabelText
                self.expensePaidAmountLabel?.text = viewModel.expensePaidAmountLabelText
                
                if !viewModel.submitToHsaButtonVisibility {
//                    constraintSubmitToHsaButtonWidth?.constant = 0
                    constraintSubmitToHSAButtonHeight?.constant = 0
                    constraintSubmitToHSAButtonHeight?.active = true
                }else{
                    constraintSubmitToHSAButtonHeight?.active = false
                }
                
                if viewModel.expenseEditButtonVisiblity
                {
                    self.expenseEditButtonHeight?.active = false
                    

                }else{
                    
                    
                    self.expenseEditButtonHeight?.constant = 0
                    self.expenseEditButtonHeight?.active = true
                }
                
                if !viewModel.expensePaidStatusLabelVisibility {
                    constraintExpensePaidAmountLabelHeight?.constant = 0
                    constraintExpensePaidStatusLabelHeight?.constant = 0
                }else{
                    constraintExpensePaidAmountLabelHeight?.constant = 22
                    constraintExpensePaidStatusLabelHeight?.constant = 15
                }
                
                self.expenseAmountLabel?.textColor = viewModel.expenseAmountLabelTextColor
                self.expenseStatusLabel?.textColor = viewModel.expenseStatusLabelTextColor
                self.expenseRemainsAmountLabel?.textColor = viewModel.expenseRemainsAmountLabelTextColor
                self.expenseRemainsStatusLabel?.textColor = viewModel.expenseStatusLabelTextColor
                self.expensePaidAmountLabel?.textColor = viewModel.expensePaidAmountLabelTextColor
                self.expensePaidStatusLabel?.textColor = viewModel.expensePaidStatusLabelTextColor
                

                
            }
            
        }
    }

 
}
