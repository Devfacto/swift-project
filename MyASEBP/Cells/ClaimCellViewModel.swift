//
//  ClaimCellViewModel.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-15.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class ClaimCellViewModel: NSObject {
    
    var amountLabelTextColor: UIColor = UIColor.statusColorForNormal()
    var statusLabelTextColor: UIColor = UIColor.statusColorForNormal()
    var amountLabelText: String?
    var statusLabelText: String?
    
    var amountNotCoveredLabelText: String = ""
    var amountNotCoveredStatusLabelText: String = "Remaining"
    var amountNotCoveredstatusLabelVisibility: Bool = false
    var amountNotCoveredLabelVisibility: Bool = false
    
    
    var editButtonVisibility: Bool = false
    
    var submitToHsaButtonVisibility: Bool = true
    
    var amountExpensedLabelText: String?
    var expenseStatusLabelText: String?
    var expenseAmountLabelText: String?
    var expenseRemainsAmountLabelText: String?
    var expensePaidAmountLabelText: String?
    
    var expenseEditButtonVisiblity: Bool = false
    var expensePaidAmountLabelVisibility: Bool = true
    var expensePaidStatusLabelVisibility: Bool = true
    
    var expenseAmountLabelTextColor: UIColor = UIColor.statusColorForNormal()
    var expenseStatusLabelTextColor: UIColor = UIColor.statusColorForNormal()
    var expenseRemainsAmountLabelTextColor: UIColor = UIColor.statusColorForNormal()
    var expenseRemainsStatusLabelTextColor: UIColor = UIColor.statusColorForNormal()
    var expensePaidAmountLabelTextColor: UIColor = UIColor.statusColorForNormal()
    var expensePaidStatusLabelTextColor: UIColor = UIColor.statusColorForNormal()
    
    var claim:Claim
    
    init(claim: Claim)
    {
        self.claim = claim
      
        statusLabelText = claim.status?.description
        amountLabelText = (claim.amount?.doubleValue.inCurrencyFormat())!

    
        
        super.init()
        
        setAmountAndStatusLabelColor()
        
        hideEditButtonIfCutOffTimePassed()
        
        hideNotCoveredLabelForFullyCoveredClaim()
        
        
        if let expense = claim.expense
        {
            submitToHsaButtonVisibility = false
            
            amountExpensedLabelText = expense.amountCovered?.doubleValue.inCurrencyFormat()
            expenseStatusLabelText = expense.status?.description
            
            
            expenseAmountLabelText = expense.amount!.doubleValue.inCurrencyFormat()
            expenseRemainsAmountLabelText = expense.amountNotCovered!.doubleValue.inCurrencyFormat()
            expensePaidAmountLabelText = expense.amountCovered?.doubleValue.inCurrencyFormat()
            
            
            if expense.status!.positivity == .Negative {
                expenseAmountLabelTextColor = UIColor.statusColorForNegative()
                expenseStatusLabelTextColor = UIColor.statusColorForNegative()
                expenseRemainsAmountLabelTextColor = UIColor.statusColorForNegative()
                expenseRemainsStatusLabelTextColor = UIColor.statusColorForNegative()
                expensePaidAmountLabelTextColor = UIColor.statusColorForNegative()
                expensePaidStatusLabelTextColor = UIColor.statusColorForNegative()
                
            }else if expense.status!.positivity == .Positive {
                expenseAmountLabelTextColor = UIColor.statusColorForPositive()
                expenseStatusLabelTextColor = UIColor.statusColorForPositive()
                expenseRemainsAmountLabelTextColor = UIColor.statusColorForPositive()
                expenseRemainsStatusLabelTextColor = UIColor.statusColorForPositive()
                expensePaidAmountLabelTextColor = UIColor.statusColorForPositive()
                expensePaidStatusLabelTextColor = UIColor.statusColorForPositive()
                
            }else{
                expenseAmountLabelTextColor = UIColor.statusColorForNormal()
                expenseStatusLabelTextColor = UIColor.statusColorForNormal()
                expenseRemainsAmountLabelTextColor = UIColor.statusColorForNormal()
                expenseRemainsStatusLabelTextColor = UIColor.statusColorForNormal()
                expensePaidAmountLabelTextColor = UIColor.statusColorForNormal()
                expensePaidStatusLabelTextColor = UIColor.statusColorForNormal()
                
            }
            
            
            if expense.amountNotCovered == 0
                || expense.amountCovered == 0{
                expensePaidAmountLabelVisibility = false
                expensePaidStatusLabelVisibility = false
                
            }else{
                expensePaidAmountLabelVisibility = true
                expensePaidStatusLabelVisibility = true
                
            }
            
            

            
            if let editCutOffTime = expense.editDeleteCutoffTimeUtc
            {
                if editCutOffTime.laterDate(NSDate()) == editCutOffTime
                {
                    expenseEditButtonVisiblity = true
                    
                }else{
                    
                    expenseEditButtonVisiblity = false
                    
                }
            }
            
            
        }


    }
    
    func hideNotCoveredLabelForFullyCoveredClaim()
    {
        if case .Processed = claim.status!
        {
            
            if claim.amountCovered!.decimalNumberByAdding(claim.cobAmount!).compare(claim.amount!) == .OrderedSame {
                amountNotCoveredLabelVisibility = false
                amountNotCoveredstatusLabelVisibility = false
                
            }else{
                amountNotCoveredLabelVisibility = true
                amountNotCoveredstatusLabelVisibility = true
                
                
            }
            
            
        }
    }
    
    func hideEditButtonIfCutOffTimePassed()
    {
        
        if let editCutOffTime = claim.editDeleteCutoffTimeUtc
        {
            if editCutOffTime.laterDate(NSDate()) == editCutOffTime
            {
                editButtonVisibility = true
                
            }else{
                
            }
        }
    }
    
    func setAmountAndStatusLabelColor()
    {
        if claim.status!.positivity == .Negative {
            amountLabelText = claim.amountNotCovered!.doubleValue.inCurrencyFormat()
            amountNotCoveredLabelText = (claim.amountNotCovered?.doubleValue.inCurrencyFormat())!
            amountLabelTextColor = UIColor.statusColorForNegative()
            statusLabelTextColor = UIColor.statusColorForNegative()
            
            
        }else if claim.status!.positivity == .Positive  {
            amountLabelText = claim.amountCovered?.decimalNumberByAdding(claim.cobAmount!).doubleValue.inCurrencyFormat()
            amountNotCoveredLabelText = claim.amountNotCovered!.decimalNumberBySubtracting(claim.cobAmount!).doubleValue.inCurrencyFormat()
            amountLabelTextColor = UIColor.statusColorForPositive()
            statusLabelTextColor = UIColor.statusColorForPositive()
            
        }
    }

}
