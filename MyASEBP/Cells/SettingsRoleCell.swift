//
//  SettingsRoleCell.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-26.
//  Copyright © 2016 devfacto. All rights reserved.
//


import UIKit

class SettingsRoleCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkmarkImageView: UIImageView!
    
}

