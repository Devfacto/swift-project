//
//  SingleTextFieldCell.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-17.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class SingleLabelCell: UITableViewCell, Validatable {

    var validationState: ValidationStatus? {
        willSet {
            if let v = newValue {
                switch v {
                case let .Valid(text):
                    titleLabel.textColor = UIColor.blackColor()
                    titleLabel.text = text
                    fieldImage.highlighted = false
                case let .Error(text, _):
                    titleLabel.textColor = UIColor(red: 0.984, green: 0.388, blue: 0.435, alpha: 1)
                    titleLabel.text = text
                    fieldImage.highlighted = true
                case .Empty:
                    titleLabel.textColor = UIColor.lightGrayColor()
                    titleLabel.text = placeholder
                    fieldImage.highlighted = false
                }
            }
        }
    }
    
    var disabled: Bool? {
        willSet {
            titleLabel.enabled = !(newValue ?? false )
        }
    }
    
    var placeholder: String?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fieldImage: UIImageView!
    
}
