//
//  NoClaimsCell.swift
//  MyASEBP-iOS
//
//  Created by Stephen Visser on 2016-07-15.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class NoClaimsCell: UITableViewCell {
    
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    enum State {
        case Normal
        case Retrying
    }
    
    var retryCallback: (() -> Void)!
    var state: State = .Normal {
        willSet {
            switch newValue {
            case .Normal:
                activityIndicator.stopAnimating()
                retryButton.hidden = false
            case .Retrying:
                activityIndicator.startAnimating()
                retryButton.hidden = true
            }
        }
    }
    
    @IBAction func retry(sender: AnyObject) {
        state = .Retrying
        retryCallback()
    }
}