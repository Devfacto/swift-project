//
//  NewClaimReceiptPhotoCell.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-11.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class NewClaimReceiptPhotoCell: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView!

    var attachmentId: String!

}
