//
//  AccountCell.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-01.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class AccountCell: UITableViewCell {

    
 
    @IBOutlet weak var constraintHsaViewWidthFull: NSLayoutConstraint!
    @IBOutlet weak var constraintHsaViewWidthZero: NSLayoutConstraint?
    @IBOutlet weak var constraintWsaBalanceTopDistance: NSLayoutConstraint!
    @IBOutlet weak var constraintHsaViewWidthEqual: NSLayoutConstraint!
    @IBOutlet weak var constraintHsaViewWidthRatio: NSLayoutConstraint?
    @IBOutlet weak var constraintHsaProgressViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintWsaProgressViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var wsaProgressView: UIProgressView!
    
    @IBOutlet weak var hsaProgressView: UIProgressView!
    
    @IBOutlet weak var hsaTotalDepositedAmountLabel: UILabel!
    @IBOutlet weak var hsaCurrentBalanceLabel: UILabel!
    @IBOutlet weak var wsaTotalDepositedAmountLabel: UILabel!
    @IBOutlet weak var wsaCurrentBalanceLabel: UILabel!
    @IBOutlet weak var wsaBalanceLabel: UILabel!
    
    @IBOutlet weak var coverageTitleLabel: UILabel!
    @IBOutlet weak var coveragePerioidLabel: UILabel!
    
    @IBOutlet weak var accountInfoView: UIView!
 

    var hideAccountTypeLabel: Bool = false {
        didSet{
            if let hsaView = hsaView
            {
                hsaView.hideAccountTypeTitleLabel = true
            }
            if let wsaView = wsaView
            {
                wsaView.hideAccountTypeTitleLabel = true
            }
            
            
        }
    }
    var hsaView:AccountBalanceProgressView?
    var wsaView:AccountBalanceProgressView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        if let hsaView = self.viewWithTag(1) as? AccountBalanceProgressView
        {
            hsaView.type = .HSA
            self.hsaView = hsaView
            
        }

        if let wsaView = self.viewWithTag(2) as? AccountBalanceProgressView
        {
            wsaView.type = .WSA
            self.wsaView = wsaView
            
        }
        
        

        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    var showHsaBalance:Bool = false
    var showWsaBalance:Bool = false
    
    var coveragePeriod:CoveragePeriod? = nil {
        didSet  {
            
            if let coveragePeriod = coveragePeriod
            {
                if coveragePeriod.status == "Active"
                {
                    self.coverageTitleLabel.text = "Current Year"
                    self.contentView.alpha = 1.0
                }else{
                    self.coverageTitleLabel.text = "Prior Year"
                    
                    self.contentView.alpha = 0.5
                }
                
                
                self.coveragePerioidLabel.text = "(\(coveragePeriod.firstActiveDate.fullDateFormat()) to \(coveragePeriod.lastActiveDate.fullDateFormat()))"
                
                
                constraintHsaViewWidthFull?.active = false
                constraintHsaViewWidthZero?.active = false
                constraintHsaViewWidthRatio?.active = false
                
                if showHsaBalance {
                    if let hsa = coveragePeriod.hsa
                    {
                        hsaView?.accountInfo = AccountInfo(balance: hsa.currentBalance, total: hsa.totalDepositedAmount)
                        
                        
                        if showWsaBalance{
                            // TODO: check if wsa exist. if no. set hsawidthFull to active
                            if let _ = coveragePeriod.wsa
                            {
                                constraintHsaViewWidthRatio?.active = true
                            }else{
                                constraintHsaViewWidthFull?.active = true
                                
                            }
                        }
                        else{
                            constraintHsaViewWidthFull?.active = true
                        }
                        
                        
                    }else{
                        
                        constraintHsaViewWidthZero?.active = true
                        
                    }
                }else{
                    constraintHsaViewWidthZero?.active = true
                }
                
                
                
                if showWsaBalance{
                    if let wsa = coveragePeriod.wsa
                    {
                        wsaView?.accountInfo = AccountInfo(balance: wsa.currentBalance, total: wsa.totalDepositedAmount)
                        wsaView?.hidden = false
                    }else{
                        wsaView?.hidden = true
                    }
                }else{
                    wsaView?.hidden = true
                }
                
                
                
                /*
                if !coveragePeriod.allocationSet  {
                    
                    
                }else{
                
                    constraintWsaProgressViewHeight.constant = 10
                    constraintHsaProgressViewHeight.constant = 10
                    
                    hsaProgressView.progressTintColor = UIColor.greenProgressViewColor()
                    hsaProgressView.trackTintColor = UIColor.lightGreyProgressViewBackColor()
                    wsaProgressView.trackTintColor = UIColor.lightGreyProgressViewBackColor()
                    
                    wsaProgressView.progressTintColor = UIColor.purpleProgressViewColor()
                    
                    
                    if let hsa = coveragePeriod.hsa
                    {
                        hsaCurrentBalanceLabel.text = hsa.currentBalance.inCurrencyFormat()
                        hsaTotalDepositedAmountLabel.text = hsa.totalDepositedAmount.inCurrencyFormat()
                        hsaProgressView.progress = 1 - hsa.currentBalance /  hsa.totalDepositedAmount
                    }
                    
                   
                    
                    if let wsa = coveragePeriod.wsa
                    {
                        if wsa.allocationPercentage > 0 {
                            wsaCurrentBalanceLabel.text = wsa.currentBalance.inCurrencyFormat()
                            wsaTotalDepositedAmountLabel.text = wsa.totalDepositedAmount.inCurrencyFormat()
                            wsaProgressView.progress = 1 - wsa.currentBalance /  wsa.totalDepositedAmount
                        }else{
                            wsaBalanceLabel.text = "Acclocated"
                            wsaCurrentBalanceLabel.text = "0%"
                            wsaTotalDepositedAmountLabel.hidden = true
                            
                            constraintWsaProgressViewHeight.constant = 0
                            constraintWsaBalanceTopDistance.constant = 0
                             
                        }
                        
                    
                    }else{
                        

                        
                        constraintHsaViewWidthRatio.active = false
                        constraintHsaViewWidthEqual.active = true
                    }
                    
                    
                    
                }
                */
                
            }
            
        }
    }

}
