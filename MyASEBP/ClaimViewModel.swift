//
//  ClaimViewModel.swift
//  MyASEBP-iOS
//
//  Created by Stephen Visser on 2016-07-21.
//  Copyright © 2016 devfacto. All rights reserved.
//

import Foundation

class ClaimViewModel {
    
    var explanationOfBenefits: [AttachmentViewModel]
    var receipts: [AttachmentViewModel]
    var otherAttachments: [AttachmentViewModel]
    
    func attachmentForType(type: ClaimFormSection) -> [AttachmentViewModel] {
        switch type {
        case .EOBs: return explanationOfBenefits
        case .OtherAttachments: return otherAttachments
        case .Receipts: return receipts
        default: fatalError()
        }
    }
    
    func removeAttachementForType(type: ClaimFormSection, atIndex index: Int) {
        switch type {
        case .EOBs: explanationOfBenefits.removeAtIndex(index)
        case .OtherAttachments: otherAttachments.removeAtIndex(index)
        case .Receipts: receipts.removeAtIndex(index)
        default: fatalError()
        }
    }
    
    func addAttachmentForType(type: ClaimFormSection, attachment: AttachmentViewModel) {
        switch type {
        case .EOBs: explanationOfBenefits.append(attachment)
        case .OtherAttachments: otherAttachments.append(attachment)
        case .Receipts: receipts.append(attachment)
        default: fatalError()
        }
    }
    
    var validationErrors: [String: String]?
    
    var id: String?
    var type: ClaimType
    var recipient: RecipientViewModel?
    var serviceDescription: String?
    var providerName: String?
    var serviceDate: NSDate?
    var amount: NSDecimalNumber?
    
    init(claim: Claim) {
        
        let allAttachmentVms = claim.attachments.map { attachment -> (AttachmentType, AttachmentViewModel) in
            
            switch attachment.fileType {
            case .Image: return (attachment.type, ImageAttachmentViewModel(attachment: attachment))
            case .Pdf: return (attachment.type, PDFAttachmentViewModel(attachment: attachment))
            }
            
        }
        
        explanationOfBenefits = allAttachmentVms.lazy.filter { type, _ in type == .EOB }.map { _, vm in vm }
        receipts = allAttachmentVms.lazy.filter { type, _ in type == .Receipt }.map { _, vm in vm }
        otherAttachments = allAttachmentVms.lazy.filter { type, _ in type == .Other }.map { _, vm in vm }

        id = claim.id
        type = claim.type
        recipient = RecipientViewModel(id: claim.recipientAsebpId, name: claim.recipientName!)
        serviceDescription = claim.serviceDescription
        providerName = claim.providerName
        serviceDate = claim.serviceDate
        amount = claim.amount
        
    }
    
    func asClaim() -> Claim {
        let claim = Claim(forNewClaimWithType: type)
        
        let eob = explanationOfBenefits.map { att -> (AttachmentType, AttachmentViewModel) in (.EOB, att) }
        let r = receipts.map { att -> (AttachmentType, AttachmentViewModel) in (.Receipt, att) }
        let o = otherAttachments.map { att -> (AttachmentType, AttachmentViewModel) in (.Other, att) }
        
        claim.attachments = (eob + r + o).map { (type, att) -> Attachment in
            let fileType: FileType
            
            if att is PDFAttachmentViewModel { fileType = .Pdf }
            else if att is ImageAttachmentViewModel { fileType = .Image }
            else { fatalError() }
            
            return Attachment(type: type, fileType: fileType)
        }
        
        claim.id = id
        claim.type = type
        claim.recipientAsebpId = recipient?.id
        claim.recipientName = recipient?.name
        claim.serviceDescription = serviceDescription
        claim.providerName = providerName
        claim.serviceDate = serviceDate
        claim.amount = amount
        
        return claim
    }
    
    //Created by user
    init(type: ClaimType) {
        
        self.type = type
        
        explanationOfBenefits = []
        receipts = []
        otherAttachments = []
        
    }
    
    var isNew: Bool {
        get {
            return id == nil
        }
    }    
    
    var amountString: String? {
        return amount?.doubleValue.format(".02")
    }
    
    
    func validateDependant() -> ValidationStatus {
        
        if let recipient = self.recipient {
            if let v = validationErrors, errorMessage = v["claimForAsebpId"] {
                return .Error(invalidText: recipient.name, errorMessage: errorMessage)
            } else {
                return .Valid(validText: recipient.name)
            }
            
        } else {
            return .Empty
        }
    }
    
    func validateDescription() -> ValidationStatus {
        
        if let description = self.serviceDescription {
            if let v = validationErrors, errorMessage = v["description"] {
                return .Error(invalidText: description, errorMessage: errorMessage)
            } else if description.characters.count == 0 {
                return .Empty
            } else {
                return .Valid(validText: description)
            }
            
        } else {
            return .Empty
        }
    }
    
    func validateProvider() -> ValidationStatus {
        
        if let provider = self.providerName {
            if let v = validationErrors, errorMessage = v["providerName"] {
                return .Error(invalidText: provider, errorMessage: errorMessage)
            } else {
                return .Valid(validText: provider)
            }
            
        } else {
            return .Empty
        }
    }
    
    func validateServiceDate() -> ValidationStatus {
        
        if let dateString = self.serviceDate.map({ $0.shortDateFormat() }) {
            
            if let v = validationErrors, errorMessage = v["serviceDate"] {
                return .Error(invalidText: dateString, errorMessage: errorMessage)
            } else {
                return .Valid(validText: dateString)
            }
            
        } else {
            return .Empty
        }
    }
    
    func validateAmount() -> ValidationStatus {
        
        if let amountString = self.amount.map({ "\($0)" }) {
            if let v = validationErrors, errorMessage = v["amount"] {
                return .Error(invalidText: amountString, errorMessage: errorMessage)
            } else if Double(amountString) == 0 {
                return .Empty
            } else {
                return .Valid(validText: amountString)
            }
            
        } else {
            return .Empty
        }
    }
    
    func validateReceipt() -> ValidationStatus {
        if self.receipts.count > 0 {
            return .Valid(validText: "")
        } else {
            return .Empty
        }
    }
    
    func validateAll() -> [ValidationResult] {
        return [
            FieldValidationResult(field: .Amount, status: validateAmount()),
            ReceiptValidationResult(status: validateReceipt()),
            FieldValidationResult(field: .Provider, status: validateProvider()),
            FieldValidationResult(field: .Dependant, status: validateDependant()),
            FieldValidationResult(field: .Description, status: validateDescription()),
            FieldValidationResult(field: .ServiceDate, status: validateServiceDate())
        ]
    }
}