//
//  AccountBalanceProgressView.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-10.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

enum SpendingAccountType {
    case HSA
    case WSA
}

struct AccountInfo {
    var balance: NSDecimalNumber
    var total: NSDecimalNumber
}

class AccountBalanceProgressView: UIView {

    
    @IBOutlet weak var constraintBalanceAmountLabelTop: NSLayoutConstraint!
    @IBOutlet weak var constraintBalanceAmountLabelBottom: NSLayoutConstraint!
    @IBOutlet weak var accountTypeLabel: UILabel!

    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var balanceAmountLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    @IBOutlet weak var constraintProgressViewHeight: NSLayoutConstraint!
    @IBOutlet var view:UIView!
    
    
    @IBOutlet weak var constraintAccountTypeLabelHeight: NSLayoutConstraint!
   
    
    var type:SpendingAccountType {
        didSet{
            if type == .HSA {
                progressView.tintColor = UIColor.greenProgressViewColor()
                accountTypeLabel.text = "HSA"
            }else{
                progressView.tintColor = UIColor.purpleProgressViewColor()
                accountTypeLabel.text = "WSA"
            }
        }
    }
    
    var hideAccountTypeTitleLabel: Bool
    {
        didSet{
            if let accountTypeLabel = accountTypeLabel
            {
                accountTypeLabel.hidden = true
                constraintAccountTypeLabelHeight.constant = 0
            }
        }
    }
    
    var accountInfo: AccountInfo {
        didSet{

            
            // TODO: handle zero allocation case
            if accountInfo.total == 0 {
                balanceLabel.text = "Allocated"
                balanceAmountLabel.text = "0%"
                progressView.hidden = true
                totalAmountLabel.hidden = true
                constraintProgressViewHeight.constant = 0
                constraintBalanceAmountLabelTop.constant = -6
                
                
            }else{
                progressView.progress = Float(accountInfo.balance.doubleValue / accountInfo.total.doubleValue) + 0.001
                balanceAmountLabel.text = accountInfo.balance.doubleValue.inCurrencyFormat()
                totalAmountLabel.text = accountInfo.total.doubleValue.inCurrencyFormat()
                
                progressView.hidden = false
                totalAmountLabel.hidden = false
                constraintProgressViewHeight.constant = 10
                constraintBalanceAmountLabelTop.constant = 4
            }
        }
    }
   
    
    required init?(coder aDecoder: NSCoder) {
        
        type = .HSA
        accountInfo = AccountInfo(balance: 0,  total: 0)
        hideAccountTypeTitleLabel = false
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
  
    func loadViewFromNib() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "AccountBalanceProgressView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.addSubview(view);
        
        if hideAccountTypeTitleLabel {
            self.accountTypeLabel.hidden = true
        }
        
        constraintProgressViewHeight.constant = 10
        
    }
}
