//
//  WebApi.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-16.
//  Copyright © 2016 devfacto. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON
import RxSwift

enum WebApiResponse {
    case Error(NSError)
    case Response(JSON)
}

class WebApi {
    
    var bearerToken: String?
    
    var inflightRequests = 0
    
    let queue = dispatch_queue_create("ca.asebp.NetworkActivityIndicator", DISPATCH_QUEUE_SERIAL)
    
    static let sharedInstance = WebApi()
    
    func getBearerToken(username: String,
                        password: String,
                        completionBlock:(result:TokenResult?, error:NSError?)->Void )
    {
        
        let parm = [
            "username":username,
            "password":password,
            "grant_type":"password"
        ]
        
        _requestJSON([:], httpMethod: .POST, url: "\(Server.BearerTokenDispatcherURL)?username=\(username)&password=\(password)", parameters: parm) { result in
            
            switch result {
            case let .Error(error): completionBlock(result: nil, error: error)
            case let .Response(response): completionBlock(result: TokenResult(json: response), error: nil)
            }
            
        }
    }
    
    func getAttachmentData(attachmentId: String, claimType: String, completionBlock:(NSData?, NSURLResponse?, NSError?) -> Void)
    {
        guard let bt = self.bearerToken else {
            fatalError("Must call getBearerToken method first")
        }
        
        let headers = [
            "authorization": "Bearer \(bt)"
        ]
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        dispatch_async(queue) { self.inflightRequests += 1 }
        
        Alamofire.request(.GET,
                          "\(Server.BaseURL)/claimattachments/OnlineClaim/\(attachmentId)",
                          parameters: nil,
                            headers: headers)
            .responseData { response in
                
                dispatch_async(self.queue) {
                    self.inflightRequests -= 1
                    if self.inflightRequests == 0 {
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    }
                }
                
                print("Original URL request \(response.request) - URL response \(response.response) - Serialization response \(response.result)")
                
                completionBlock(response.data, response.response, response.result.error)
            }
                
 
    }
    
    func _requestJSON(headers: [String: String], httpMethod:Alamofire.Method, url: String,  parameters: [String: AnyObject]?, completionBlock: (WebApiResponse) -> Void)
    {

        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        dispatch_async(queue) { self.inflightRequests += 1 }
        
        Alamofire.request(httpMethod, url, parameters: parameters, headers: headers).responseJSON { [unowned self] response in
            
            dispatch_async(self.queue) {
                self.inflightRequests -= 1
                if self.inflightRequests == 0 {
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                }
            }
            
            print("Original URL request \(response.request) - URL response \(response.response) - Serialization response \(response.result)")
        
//            #if DEBUG
//            if arc4random_uniform(5) < 1 {
//                completionBlock(.Error(NSError(domain: "", code: 1337, userInfo: [NSLocalizedDescriptionKey: "Fake intermittent connection"])))
//                return
//            }
//            #endif
            
            if let error = response.result.error {
                if response.response?.statusCode == 401
                {
                    completionBlock(.Error(NSError(domain: "", code: 401, userInfo: [NSLocalizedDescriptionKey: "Invalid user name or password"])))
                }else{
                    completionBlock(.Error(error))
                }
                
            } else {
        
                //TODO: What happens when WS doesn't return an NSDictionary or JSON-friendly type?
                if let json = response.result.value.map(JSON.init)  {
                    print(json)
                    completionBlock(.Response(json))

                } else {
                    
                    completionBlock(.Error(NSError(domain: "", code: 1000, userInfo: [NSLocalizedDescriptionKey: "Error during response deserialization"])))
                    
                }
            }
        }
    }
    
    func requestJSON(httpMethod:Alamofire.Method = Method.GET, url: String,  parameters: [String: AnyObject]? = nil, completionBlock: (WebApiResponse) -> Void) {
        guard let bt = self.bearerToken else {
            fatalError("Must call getBearerToken method first")
        }
        
        let headers = [
            "Authorization": "Bearer \(bt)",
            "Accept": "application/json"
        ]

        _requestJSON(headers, httpMethod: httpMethod, url: url, parameters: parameters, completionBlock: completionBlock)
    }
    
    
    func getOne<T where T:JSONConstructable>(httpMethod:Alamofire.Method = Method.GET, url: String,  parameters: [String: AnyObject]? = nil, completionBlock:(result:T?, error:NSError?) -> Void)
    {
        requestJSON(httpMethod, url: url) { result in
            switch result {
            case let .Error(error): completionBlock(result: nil, error: error)
            case let .Response(result): completionBlock(result: T(json: result), error: nil)
            }
        }
 
    }
    
    func getDictionary(httpMethod:Alamofire.Method = Method.GET,  url: String, parameters: [String: AnyObject]? = nil, completionBlock:(results:[String: AnyObject]?, error:NSError?) -> Void)
    {
        
        requestJSON(httpMethod, url: url, parameters: parameters) { result in
            
            switch result {
            case let .Error(error): completionBlock(results: nil, error: error)
            case let .Response(result):
                if let r = result.dictionaryObject {
                    completionBlock(results: r, error: nil)
                } else {
                    completionBlock(results: nil, error: NSError(domain: "", code: 1001, userInfo: [NSLocalizedDescriptionKey: "Expected a dictionary, but was missing"]))
                }
            }
        }
      
    }
    
    func getList(url: String, parameters: [String: AnyObject]? = nil, completionBlock:(results:[AnyObject]?, error:NSError?) -> Void)
    {
        requestJSON(.GET, url: url, parameters: parameters) { result in
            switch result {
            case let .Error(error): completionBlock(results: nil, error: error)
            case let .Response(result):
                if let r = result.arrayObject {
                    completionBlock(results: r, error: nil)
                } else {
                    completionBlock(results: nil, error: NSError(domain: "", code: 1001, userInfo: [NSLocalizedDescriptionKey: "Expected an array, but was missing"]))
                }
            }
        }
    }
    
    func getMemberInfo(asebpId: String, completionBlock:(member:Member?, error:NSError?)->Void )
    {
       
        getOne(url:"\(Server.BaseURL)/person/\(asebpId)", completionBlock: {
        
            (member, error) in
            
          
                completionBlock(member: member, error: error)
        })
    }
    
    
    func getProviderList(asebpId: String, completionBlock:(results:[AnyObject]?, error:NSError?)->Void )
    {
        
        getList("\(Server.BaseURL)/members/\(asebpId)/providerNames/recent", completionBlock:completionBlock)
    }
    
    func searchProviderByKeyword(asebpId: String, keyword: String, completionBlock:(results:[AnyObject]?, error:NSError?)->Void )
    {
        
        getList("\(Server.BaseURL)/members/\(asebpId)/providerNames",
                parameters: ["search": keyword],
                completionBlock:completionBlock)
    }

    
    
    func searchServiceDescriptionByKeyword(asebpId: String,
                                            keyword: String,
                                            completionBlock:(results:[AnyObject]?, error:NSError?)->Void )
    {
        
        getList("\(Server.BaseURL)/members/\(asebpId)/serviceDescriptions",
                parameters: ["search": keyword],
                completionBlock:completionBlock)
    }
    
    func getServiceDescriptionList(asebpId: String, completionBlock:(results:[AnyObject]?, error:NSError?)->Void )
    {
   
        getList("\(Server.BaseURL)/members/\(asebpId)/serviceDescriptions/recent",
                completionBlock:completionBlock)
    }
    

    func getExpenseCategoryList(asebpId: String, accountType: String, completionBlock:(results:[ExpenseCategory]?, error:NSError?)->Void )
    {
        
        getList("\(Server.BaseURL)/claims/expenseCategories",
                parameters:  ["accountType": accountType],
                completionBlock:{
        
                    (results, error)  in
                    
                    guard let results = results else {
                        fatalError("no expense category found")
                    }
                    
                        
                    let list = results.map({ (dict) -> ExpenseCategory in
                        ExpenseCategory(json: JSON(dict))
                    })
                    
                    completionBlock(results: list, error: error)
                
                    
                    
                    
        
        })
        
    }
    
    func addAttachment(claimId: String, attachment: Attachment) -> Observable<Bool> {
        NSLog("Adding attachment \(attachment.fileName) to claim \(claimId)")
        return Observable.just(arc4random_uniform(1) > 0).delaySubscription(1, scheduler: MainScheduler.instance)
    }
    
    func removeAttachment(claimId: String, attachment: Attachment) -> Observable<Bool> {
        NSLog("Removing attachment \(attachment.fileName) to claim \(claimId)")
        return Observable.just(arc4random_uniform(1) > 0).delaySubscription(1, scheduler: MainScheduler.instance)

    }
    
    func submitPost(claim: Claim) -> Observable<String> {
        NSLog("Submitting post \(claim.id)")
        return Observable.just("123456").delaySubscription(1, scheduler: MainScheduler.instance)
    }
    
    func finalizePost(claimId: String) -> Observable<Void> {
        NSLog("Finalizing post \(claimId)")
        return Observable.just().delaySubscription(1, scheduler: MainScheduler.instance)
    }
    
    func validateClaimSubmission(asebpId: String,
                                 memberId: String,
                                 claim: Claim,
                                 completionBlock:(results:[String: String]?, error:NSError?)->Void )
    {
        
  
        
        let submissionType:String
        switch claim.type {
        case .CoreClaim: submissionType = "OnlineClaim"
        case .HSA: submissionType = "HSA"
        case .WSA: submissionType = "WSA"
        }
        
        var parameters: [String : AnyObject] = [
            "type":submissionType,
            "asebpId":asebpId,
            "memberId":memberId,
            "amount":claim.amount ?? 0,
            "createdByUser":asebpId,
        ]
        if let serviceDate = claim.serviceDate
        {
            parameters["serviceDate"] =  serviceDate.fullDateTimeFormat()
        }
     
        if let providerName = claim.providerName
        {
            parameters["providerName"] =  providerName
        }
 
        if let recipientAsebpId = claim.recipientAsebpId
        {
            parameters["claimForAsebpId"] =  recipientAsebpId
        }
        
        if let description = claim.serviceDescription
        {
            parameters["description"] =  description
        }
        
        switch claim.type {
        case let .CoreClaim(type):
            parameters["onlineClaimType"] = type.rawValue
        default: break
        }

        parameters["attachments"] = claim.attachments.map { ["type": $0.type.rawValue] }
        
        print(parameters)
        
        getDictionary(.POST, url:"\(Server.BaseURL)/claims/validate",
                      parameters: parameters) { val, error in
                completionBlock(results: val as? [String: String], error: error)
        }
        
     }

    func submitClaimSubmission(asebpId: String,
                                 memberId: String,
                                 claim: Claim,
                                 completionBlock:(results:[String: String]?, error:NSError?)->Void )
    {
        
        
        
        let submissionType:String
        switch claim.type {
        case .CoreClaim: submissionType = "OnlineClaim"
        case .HSA: submissionType = "HSA"
        case .WSA: submissionType = "WSA"
        }
        
        var parameters: [String : AnyObject] = [
            "type":submissionType,
            "asebpId":asebpId,
            "memberId":memberId,
            "amount":claim.amount ?? 0,
            "createdByUser":asebpId,
            ]
        if let serviceDate = claim.serviceDate
        {
            parameters["serviceDate"] =  serviceDate.fullDateTimeFormat()
        }
        
        if let providerName = claim.providerName
        {
            parameters["providerName"] =  providerName
        }
        
        if let recipientAsebpId = claim.recipientAsebpId
        {
            parameters["claimForAsebpId"] =  recipientAsebpId
        }
        
        if let description = claim.serviceDescription
        {
            parameters["description"] =  description
        }
        
        switch claim.type {
        case let .CoreClaim(type):
            parameters["onlineClaimType"] = type.rawValue
        default: break
        }
        
        parameters["attachments"] = claim.attachments.map { ["type": $0.type.rawValue] }
        
        print(parameters)
        
        getDictionary(.POST, url:"\(Server.BaseURL)/claims/validate",
                      parameters: parameters) { val, error in
                        completionBlock(results: val as? [String: String], error: error)
        }
        
    }

    
    func getClaimList(memberId: String,
                      olderThan: NSDate? = NSDate.distantFuture(),
                      maxResults: Int? = 20,
                      completionBlock:(results:[Claim]?, error:NSError?)->Void )
    {
        
        var parm = [
            "memberId":memberId
        ]
        
        if let olderThan = olderThan
        {
            parm["olderThan"] = olderThan.fullDateTimeFormat()
        }
        
        if let maxResults = maxResults
        {
            parm["maxResults"] = String(maxResults)
        }
        
        getList("\(Server.BaseURL)/claims",
                parameters: parm,
                completionBlock:   {(results, error) in
 
                    
                    completionBlock(results: results == nil ? nil :  results!.map({ (item) -> Claim in
                        Claim(json: JSON(item))
                    }), error: error)
                    
        })
  
    }
    
    //TODO: retrieve the contact info from the api service
    func getContactInfo()
    {
        NSLog("Retrieve Contact Info")
        return
    }
}




