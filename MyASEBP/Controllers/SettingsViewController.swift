//
//  SettingsViewController.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-11.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onDone(sender: AnyObject) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onCancel(sender: AnyObject) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
 
        return 2 + (membershipCount > 1 ? 1 : 0)
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        if membershipCount > 1
        {
            return [membershipCount,1,1][section]
        }
        
        return 1
        
    }

    var membershipCount : Int {
        get {
            var membershipCount = 0
            if let membership = Member.currentMember?.memberships
            {
                membershipCount = membership.count
            }
            
            return membershipCount
        }
    }
   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {

         if membershipCount > 1
         {
            let cell = tableView.dequeueReusableCellWithIdentifier(
                ["SettingsRoleCell",
                "SettingsWalletCardCell",
                "SettingsLogoutCell"][indexPath.section], forIndexPath: indexPath)

            if indexPath.section == 0 {
                
                
                if let memberships = Member.currentMember?.memberships,
                    cell = cell as? SettingsRoleCell
                {
                    let membership = memberships[indexPath.row]
                    
                    let isSelected = ( membership.id == Member.selectedMembershipId )
                    
                    cell.textLabel!.text = membership.schoolJurisdiction
                    cell.textLabel!.enabled = isSelected
                    cell.detailTextLabel!.text = membership.schoolJurisdictionGroup
                    cell.detailTextLabel!.enabled = isSelected
                    cell.imageView!.hidden = !isSelected
                    
                }
            }
            
            
            return cell
         }
            
        return   tableView.dequeueReusableCellWithIdentifier(
            [ "SettingsWalletCardCell",
                "SettingsLogoutCell"][indexPath.section], forIndexPath: indexPath)
        
    }
 

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         if membershipCount > 1
         {
            
            return ["ROLES", "", ""][section]
        }
        
        return ""
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if membershipCount > 1
        {
            if indexPath.section == 0 {
                if let memberships = Member.currentMember?.memberships
                {
                    if Member.selectedMembershipId != memberships[indexPath.row].id {
                        Member.selectedMembershipId = memberships[indexPath.row].id
                        
                        tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
                        
                        
                        NSNotificationCenter.defaultCenter().postNotificationName(AsebpNotification.SelectedMembershipSwitchedNotifiction, object: nil)
                    }
                }
            }
            
            

        }
        
        if indexPath.section == tableView.numberOfSections - 1 {
            // logout
            (UIApplication.sharedApplication().delegate as! AppDelegate).logout()
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "SettingsToIDCard" {
            let viewController = segue.destinationViewController 
            
                viewController.navigationItem.rightBarButtonItem = nil
            
        }
        
    }
    

}
