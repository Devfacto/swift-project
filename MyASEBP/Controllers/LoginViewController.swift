//
//  LoginViewController.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-09.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import SwiftyJSON

import Crashlytics

class LoginViewController: UIViewController, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var IDCardButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var items = ["Username", "Password"]
 
  
    var asebpIdTextField:UITextField!
    var passwordTextField:UITextField!
    
    var member:Member?
 
    
    var appNavigator  = AppNavigator.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
   
        
        
        playAnimation()
        
   

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.asebpIdTextField != nil && self.passwordTextField != nil
        {
            if let memberId = Member.currentUserAsebpIdOrEmail {
                self.asebpIdTextField.text = memberId
            }else{
                self.asebpIdTextField.text = ""
            }
            
            if self.asebpIdTextField.text?.characters.count == 0
            {
                self.asebpIdTextField.becomeFirstResponder()
            }else{
                self.passwordTextField.becomeFirstResponder()
            }
        }
        
        if let _ = Member.currentMember {
            self.IDCardButton.hidden = false
        }else{
            self.IDCardButton.hidden = true
        }

        #if DEBUG
            self.passwordTextField.text = "Password1"
        #else
            self.passwordTextField.text = ""
        #endif
     }
    
    func playAnimation() {
        
        if let bannerImage = self.view.viewWithTag(1),
            loginView = self.view.viewWithTag(2)
        {
            
            
            
            var frame = bannerImage.frame
            
            frame.origin.x = ( self.view.frame.width - bannerImage.frame.width ) / 2
            frame.origin.y = ( self.view.frame.height - bannerImage.frame.height ) / 2
            bannerImage.frame = frame
            
       
            
            UIView.animateWithDuration(1.3,
                                       delay: 0.0,
                                       options: UIViewAnimationOptions.CurveLinear,
                                       animations: {
                                            frame.origin.y = 10
                                            
                                            bannerImage.frame = frame
                                        
                                        
                                        
                                        }
            ) { (finished) in
                
             }
            
            
            loginView.alpha = 0
            
            UIView.animateWithDuration(1.0,
                                       delay: 0.3,
                                       options: UIViewAnimationOptions.ShowHideTransitionViews,
                                       animations: {
 
                                        
                                        loginView.alpha = 1
                }
            ) { (finished) in
                
                
                if self.asebpIdTextField.text?.characters.count == 0
                {
                    self.asebpIdTextField.becomeFirstResponder()
                }else{
                    self.passwordTextField.becomeFirstResponder()
                }
                
                
            }

            
        }

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
       return true
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let identifier = indexPath.row == 0 ? "LoginUserNameCell" : "LoginPasswordCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! SingleTextFieldCell
        
        cell.textField.delegate = self
        
        if indexPath.row == 0 {
            self.asebpIdTextField = cell.textField
            
            if let memberId = Member.currentUserAsebpIdOrEmail
            {
                self.asebpIdTextField.text = memberId
               
            }
            
        }else{
            self.passwordTextField = cell.textField
            
        }
        
        return cell
        
        
    }
 
    
    @IBAction func onLogin(sender: AnyObject)
    {

        self.asebpIdTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
        


        
        if let asebpIdOrEmail = self.asebpIdTextField!.text
        {
            if asebpIdOrEmail.characters.count > 0 {
                
//                SVProgressHUD.show()
//                SVProgressHUD.setDefaultMaskType(.Clear)
                
                
                let button = sender as! UIButton
                
                button.setTitle("", forState: .Disabled)
                button.enabled = false
                
                let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
                spinner.startAnimating()
                spinner.center = button.convertPoint(button.center, fromView: button.superview)
                button.addSubview(spinner)
                
                self.view.userInteractionEnabled = false
                
           
                
                WebApi.sharedInstance.getBearerToken(asebpIdOrEmail.urlEncoded(),
                                      password: self.passwordTextField.text!, completionBlock: { (result, error)
                in
                                        
                                        
                    
                                        
                    if  let error = error
                    {
                        spinner.stopAnimating()
                        spinner.removeFromSuperview()
                        button.enabled = true
                        self.view.userInteractionEnabled = true
                        
                        let alertController = UIAlertController(title: "Error",
                            message: error.localizedDescription, preferredStyle: .Alert)
                        
                        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                    }
                    
                    if let result = result
                    {
                        print(result._json)
                        
                        WebApi.sharedInstance.bearerToken = result.accessToken

                        
                        WebApi.sharedInstance.getMemberInfo(result.asebpId, completionBlock: { (member, error) in
                            
                            spinner.stopAnimating()
                            spinner.removeFromSuperview()
                            button.enabled = true
                            self.view.userInteractionEnabled = true
                            
                            
                            
                            if let error =  error
                            {
                                NSLog(error.localizedDescription)
                                
                                let ac = UIAlertController(title: "Error",
                                    message: error.localizedDescription,
                                    preferredStyle: .Alert)
                                
                                ac.addAction(UIAlertAction(title: "Ok",
                                    style: .Default,
                                    handler: nil))
                                
                                self.presentViewController(ac, animated: true, completion: nil)
                            }
                            
                            if let member = member
                            {
                 
                                print(member._json)
                                
                                self.logUser(member)
                                
                                Member.currentMember = member
                                Member.currentUserAsebpIdOrEmail = asebpIdOrEmail
                                
                                self.appNavigator.showMainScreen(self)
                                            
                                self.passwordTextField.text = ""
                                
                                
                                
                            }
                            
                            
                            
                        })
                        
 
                    }
                
                            
                    

                })
                
               
   
            }else{
                
                self.tableView.reloadData()
            }
        
        }
       
        
    }
    
    func logUser(member: Member) {
        
        Crashlytics.sharedInstance().setUserIdentifier(member.id)
        
        Crashlytics.sharedInstance().setUserName(member.fullName)
    }
    
    
    // MARK: - UITextField
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        if textField == passwordTextField
        {
            onLogin(self.signInButton)
        }
        
        return true
    }
}
