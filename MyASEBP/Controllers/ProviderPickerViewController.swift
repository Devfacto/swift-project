//
//  ProviderPickerViewController.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-31.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class ProviderPickerViewController: StringPickerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func reloadData() {
        
        WebApi.sharedInstance.getProviderList(Member.currentMember!.id!) { (results, error) in
            
            
            if  let results = results {
                
                self.items = results.map({ (result) -> String in
                    result as! String
                })
                
                self.tableView.reloadData()
             
                
            }
            
            if let error = error {
                
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            // TODO: handle error
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override var filteredItems:[String] {
        get{
            if let stringToSearch = (self.searchBar != nil) ? self.searchBar!.text : ""
            {
                if stringToSearch.characters.count > 0
                {
                    return items.filter({ (item) -> Bool in
                        return item.localizedCaseInsensitiveContainsString(stringToSearch)
                    })
                }
                
            }
            
            return items
            
        }
    }
    
    // MARK: - UISearchBarDelegate
    override func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if  searchText.characters.count > 0 //3 TODO: if too many hits return, only do search for more than 3 characters typed
        {
        
            WebApi.sharedInstance.searchProviderByKeyword(Member.currentMember!.id!,
                                               keyword: searchText) { (results, error) in
                                                
                                                if let error = error
                                                {
                                                   NSLog("searchProviderListByKeyword error: %@", error)
                                                }
                                                
                                                if let results = results
                                                {
                                                    self.items = results.map({ (provider) -> String in
                                                        provider as! String
                                                    })
                                                    
                                                    self.tableView.reloadData()
                                                }
                                                
            }
            
        }else{
            if  searchText.characters.count == 0
            {
                self.reloadData()
            }
        }
        
        
    }

}
