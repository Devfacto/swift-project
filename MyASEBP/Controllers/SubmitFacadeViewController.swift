//
//  SubmitFacadeViewController.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-08.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class SubmitFacadeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var constraintHsaButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintHsaButtonWidthFull: NSLayoutConstraint!

    @IBOutlet weak var constraintHsaButtonWidthZero: NSLayoutConstraint!
    @IBOutlet weak var constraintWsaButtonWidthZero: NSLayoutConstraint!
    @IBOutlet weak var constraintHsaButtonWidthRatio: NSLayoutConstraint!
    @IBOutlet weak var constraintWsaButtonWidthRatio: NSLayoutConstraint!
    
    
    @IBOutlet weak var hsaButton: UIButton!
    @IBOutlet weak var wsaButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submissionBannerView: UIView!
    @IBOutlet weak var submissionBannerTextView: UILabel!

    var submission: String? {
        willSet {
            if let s = newValue {
                submissionBannerTextView.text = s
                submissionBannerView.hidden = false
            } else {
                submissionBannerView.hidden = true
            }
        }
    }
    
    @IBAction func dismissSubmissionBanner(sender: AnyObject) {
        submission = nil
    }
    
    enum SubmisstionType {
        case EHC
        case VIS
        case HSA
        case WSA
    }
    
    var rows:[SubmisstionType] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
  
        
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                     selector: #selector(SubmitFacadeViewController.viewWillLayoutSubviews),
                                                     name: AsebpNotification.MembershipDetailInfoLoadedNotifiction,
                                                     object: nil)
        
        submission = nil
    }
    
    override func viewDidDisappear(animated: Bool) {
        submission = nil
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        

        rows = []

        if let selectedMembership =  Member.selectedMembership
        {
            if  selectedMembership.hasEHC {
                rows.append(.EHC)
            }
     
            if  selectedMembership.hasVision {
                rows.append(.VIS)
            }
            
            
            if  selectedMembership.allowHsa {
                rows.append(.HSA)
            }
            
            if selectedMembership.allowWsa {
                rows.append(.WSA)
            }
        }

        

        
        self.tableView.reloadData()
    }
    
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "" {
             return true
        }
        return false
    }

    
    @IBAction func onReadMore(sender: UIButton)
    {
        
        guard let cell = sender.parentTableViewCell,
        let indexPath = self.tableView.indexPathForCell(cell) else
        {
            return
        }
        
        switch rows[indexPath.row] {
        case .EHC:
            AppNavigator.sharedInstance.openEHCInfoLink()
        case .VIS:
            AppNavigator.sharedInstance.openVisionInfoLink()
        case .HSA:
            AppNavigator.sharedInstance.openHSAInfoLink()
        case .WSA:
            AppNavigator.sharedInstance.openWSAInfoLink()
            
        }
    
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let identifier = [ .EHC: "NewClaimToEHC",
                           .VIS: "NewClaimToVision",
                           .HSA: "NewClaimToHsaDetails",
                           .WSA: "NewClaimToWsaDetails"][rows[indexPath.row]]
        
        
            performSegueWithIdentifier(identifier!, sender: nil)

    }
    

   
    // MARK: - Navigation

    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if sender != nil  {
            return
        }
            // 
            if let nvc = segue.destinationViewController as? UINavigationController
            {
                
                if let viewController = nvc.viewControllers.first as? NewExpenseViewController
                {
                    print("loading new expense")
                    if segue.identifier == "NewClaimToWsaDetails" {
                        viewController.claim = Claim(forNewClaimWithType: .WSA)
                    }else if segue.identifier == "NewClaimToHsaDetails" {
                        viewController.claim = Claim(forNewClaimWithType: .HSA)
                    }

                }else if let viewController = nvc.viewControllers.first as? NewClaimViewController
                {
                    print("loading new claim")

                    if segue.identifier == "NewClaimToEHC" {
                       viewController.claim = ClaimViewModel(type: .CoreClaim(.ExtendedHealthCare))
                    }else if segue.identifier == "NewClaimToVision" {
                        viewController.claim = ClaimViewModel(type: .CoreClaim(.Vision))
                    }
                }
                
            }
            
        
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return rows.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = [ .EHC: "SubmissionTypeEHCCell",
                           .VIS: "SubmissionTypeVisionCell",
                           .HSA: "SubmissionTypeHSACell",
                           .WSA: "SubmissionTypeWSACell"][rows[indexPath.row]]
        
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier!, forIndexPath: indexPath)

        
        return cell
    }
    
    @IBAction func submit(segue: UIStoryboardSegue) {
        if let _ = segue.sourceViewController as? NewClaimViewController {
            submission = "Submitted Core Claim"
        }
    }
    
    @IBAction func cancel(segue: UIStoryboardSegue) {
    }

}
