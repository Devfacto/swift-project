//
//  ClaimListViewController.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-26.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import IDMPhotoBrowser
import SwiftyJSON
import CCBottomRefreshControl

class ClaimListViewController: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var noConnectionView: UIView!
    @IBOutlet weak var noConnectionActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noConnectionRefreshButton: UIButton!
    
    enum ConnectionFailureState {
        case NoFailure
        case Failure
        case Retrying
    }
    
    enum NoItemsState {
        case ItemsPresent
        case NoItems
        case Retrying
    }
    
    var retryCell: NoClaimsCell? {
        get {
            return tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 1)) as? NoClaimsCell
        }
    }
    
    var connectionFailureState: ConnectionFailureState = .NoFailure {
        willSet {
            
            switch newValue {
            case .NoFailure:
                self.noConnectionView.hidden = true
                self.noConnectionActivityIndicator.stopAnimating()
            case .Failure:
                self.noConnectionView.hidden = false
                self.noConnectionActivityIndicator.stopAnimating()
                self.noConnectionRefreshButton.hidden = false
            case .Retrying:
                self.noConnectionActivityIndicator.startAnimating()
                self.noConnectionRefreshButton.hidden = true
            }
            
        }
    }
    
    
    var refreshControl: UIRefreshControl!
    
    var coveragePeriods: [CoveragePeriod]?
    var items : [Claim]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 120
        
 
        loadData()
        
        connectionFailureState = .NoFailure
        
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(ClaimListViewController.resetList),
                                                         name: AsebpNotification.SelectedMembershipSwitchedNotifiction,
                                                         object:nil)
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = UIColor.whiteColor()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self,
                                      action: #selector(ClaimListViewController.loadData),
                                      forControlEvents: UIControlEvents.ValueChanged)
        
        self.tableView.addSubview(self.refreshControl) // not required when using UITableViewController
        
        
        let refreshControl = UIRefreshControl()
        refreshControl.triggerVerticalOffset = 100
        refreshControl.addTarget(self,
                                 action: #selector(ClaimListViewController.loadMoreHistoryData),
                                 forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.bottomRefreshControl = refreshControl

    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ()
 
    func loadMoreHistoryData(refreshControl: UIRefreshControl)
    {
        guard let items = self.items else {
            return
        }
 
        WebApi.sharedInstance.getClaimList(Member.selectedMembershipId!,
                            olderThan: items.last?.serviceDate,
                            maxResults: 20
            
        
        ) { (results, error) in
            
            if refreshControl.refreshing
            {
                refreshControl.endRefreshing()
            }
            
            if let results = results
            {
                
                self.items = (self.items ?? []) + results
                
                self.tableView.reloadData()
            }
            
            
            if let error = error
            {
                NSLog("Error getting claim list \(error.localizedDescription)")
                self.connectionFailureState = .Failure
                
            } else {
                self.connectionFailureState = .NoFailure
            }
            
        }
    }
    
    func resetList()
    {
        coveragePeriods = nil
        items = nil
        self.tableView.reloadData()
        
        loadData()
    }
    
    func loadData()
    {
        var memberId = Member.selectedMembershipId!
        
        
        if let memberships = Member.currentMember?.memberships{
            if memberships.count > 0
                && memberships.filter({ (membership) -> Bool in
                
                membership.id == memberId
                }
                
                ).count == 0
            {
                    
                    if let newMemberId = Member.currentMember!.memberships!.first!.id
                    {
                        Member.selectedMembershipId = newMemberId
                        
                        memberId = newMemberId
                    }
                    
            }
        }
       
        WebApi.sharedInstance.getMemberInfo(Member.currentMember!.id!, completionBlock: { (member, error) in
            
            //TODO: Create special cell for expense fetching failure
            
            if let member = member
            {
                print(member._json)
                Member.currentMember = member
               
                self.coveragePeriods = Member.selectedMembership?.coveragePeriods ?? []
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
                
                NSNotificationCenter.defaultCenter().postNotificationName(AsebpNotification.MembershipDetailInfoLoadedNotifiction, object: nil)
            }
            
        })
        
        
        WebApi.sharedInstance.getClaimList(memberId) { (results, error) in
            
            if self.refreshControl.refreshing
            {
                self.refreshControl.endRefreshing()
            }
            
            if error != nil {
                self.connectionFailureState = .Failure
            } else {
                self.connectionFailureState = .NoFailure
            }
            
            if let retryCell = self.retryCell {
                retryCell.state = .Normal
            }
            
            if let results = results
            {
                self.items = results
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }

        }
    }
    

    
    
    // MARK: - IBActions
    @IBAction func editExpense(sender: UIButton)
    {
        if let cell = sender.parentTableViewCell as? ClaimCell {
            
            if case .CoreClaim = cell.claim.type
            {
                if let viewController = AppNavigator.sharedInstance.expenseDetailsVC
                {
                    let nvc = UINavigationController(rootViewController: viewController)
                    
                    viewController.claim = cell.claim?.expense
                    
                    self.navigationController?.presentViewController(nvc, animated: true, completion: {
                        
                    })
                }
                
            } 
            
            
        }
    }

    
    @IBAction func editClaim(sender: UIButton)
    {
        if let cell = sender.parentTableViewCell as? ClaimCell {
            
            switch cell.claim.type {
            case .HSA, .WSA:
                if let viewController = AppNavigator.sharedInstance.expenseDetailsVC
                {
                    let nvc = UINavigationController(rootViewController: viewController)
                    
                    viewController.claim = cell.claim
                    
                    self.navigationController?.presentViewController(nvc, animated: true, completion: {
                        
                    })
                }
            case .CoreClaim:
                if let viewController = AppNavigator.sharedInstance.claimDetailsVC
                {
                    let nvc = UINavigationController(rootViewController: viewController)
                    
                    viewController.claim = ClaimViewModel(claim: cell.claim)
                    viewController.originalClaim = cell.claim
                    
                    self.navigationController?.presentViewController(nvc, animated: true, completion: {
                        
                    })
                }
            }
            
            
        }
    }
    
    @IBAction func reviewImages(sender: UIButton)
    {
        editClaim(sender)
    }
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        // fix for sometimes refreshing not triggering, refresh control bug?
        // https://possiblemobile.com/2014/05/ios-custom-pull-to-refresh/
        let minOffsetToTriggerRefresh = CGFloat(50.0)
        if (scrollView.contentOffset.y <= -minOffsetToTriggerRefresh) {
            
            if !self.refreshControl.refreshing
            {
                self.refreshControl.beginRefreshing()
                self.loadData()
            }
            
        }
    }
 
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch (self.items?.count, self.coveragePeriods?.count) {
        case let (claimCount?, _) where section == 1 && claimCount == 0: return 1
        case let (claimCount?, _) where section == 1 && claimCount > 0: return claimCount
        case let (_, coveragePeriodCount?) where section == 0: return coveragePeriodCount
        default: return section == 0 ? 0 : 8
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0 {
            
            if let cp = self.coveragePeriods?[indexPath.row] {
            
                var identifier = "AccountCell"
                
                if !cp.allocationSet {
                    identifier = "AccountCellNoAllocation"
                }
                
                let cell = tableView.dequeueReusableCellWithIdentifier(
                    identifier, forIndexPath: indexPath) as! AccountCell
                
                cell.showHsaBalance = true
                cell.showWsaBalance = true
                
                cell.coveragePeriod = cp
                
                return cell
            } else {
                return tableView.dequeueReusableCellWithIdentifier("PlaceholderCell", forIndexPath: indexPath)
            }
        }
        
        if let items = self.items {
            
            if items.count == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("NoClaimsCell", forIndexPath: indexPath) as! NoClaimsCell
                cell.retryCallback = loadData
                return cell
            } else {
            
                let claim = items[indexPath.row]
                
                let identifier: String
                
                switch claim.state {
                case .CandidateForExpense: identifier = "ClaimCellPartiallyOrNotCovered"
                case .Editable: identifier = "ClaimCellPendingEditable"
                case .HasNotYetProcessedExpense: identifier = "ClaimCellHSASubmitted"
                case .HasProcessedExpense: identifier = "ClaimCellHSAProcessed"
                case .NotEditable: identifier = "ClaimCellPendingNotEditable"
                }

                let cell = tableView.dequeueReusableCellWithIdentifier(
                    identifier, forIndexPath: indexPath) as! ClaimCell
                cell.claim = claim
                    
                return cell
                
            }
        } else {
            return tableView.dequeueReusableCellWithIdentifier("PlaceholderCell", forIndexPath: indexPath)
        }
    }

    @IBAction func refreshAfterFailure(sender: AnyObject) {
        self.connectionFailureState = .Retrying
        self.loadData()
    }
    
}
