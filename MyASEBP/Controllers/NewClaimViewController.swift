//
//  NewClaimViewController.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-10.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import SVProgressHUD
import MobileCoreServices
import IDMPhotoBrowser
import RxSwift

enum ValidationStatus {
     case Valid(validText: String)
     case Error(invalidText: String, errorMessage: String)
     case Empty
}

protocol ValidationResult {
     var status: ValidationStatus { get }
}

struct FieldValidationResult: ValidationResult {
     let field: ClaimFormField
     let status: ValidationStatus
}

struct ReceiptValidationResult: ValidationResult {
     let status: ValidationStatus
}

enum ClaimFormField {
     case Description
     case Provider
     case Amount
     case ServiceDate
     case Dependant
     case DatePicker
}

enum ClaimFormSection {
     case BasicInfo(fields: [ClaimFormField])
     case EOBs
     case Receipts
     case OtherAttachments
     case Delete
}

enum PickerSenderTag : Int {
     case Patient = 0
     case ServiceDescription = 1
     case Provider = 2
     case ExpenseCategory = 3
}

enum FieldImage {
     case Amount
     case Description
     case ServiceDate
     case Provider
     case Dependant
     
     var errorImage: UIImage {
          switch self {
          case .Amount: return UIImage(named: "How Much Icon Red")!
          case .Dependant: return UIImage(named: "Who Icon Red")!
          case .Description: return UIImage(named: "What Icon Red")!
          case .ServiceDate: return UIImage(named: "When Icon Red")!
          case .Provider: return UIImage(named: "Where Icon Red")!
          }
     }

     var validImage: UIImage {
          switch self {
          case .Amount: return UIImage(named: "How Much Icon Grey")!
          case .Dependant: return UIImage(named: "Who Icon Grey")!
          case .Description: return UIImage(named: "What Icon Grey")!
          case .ServiceDate: return UIImage(named: "When Icon Grey")!
          case .Provider: return UIImage(named: "Where Icon Grey")!
          }
     }
}

protocol Validatable {
     var validationState: ValidationStatus? { get set }
     var disabled: Bool? { get set }
}

class ClaimFieldsDataSource: NSObject, UITableViewDataSource {
     
     var isInDatePickingMode = false
     var disabled = false
    
     let claim: ClaimViewModel
     var didRemoveItem: ((AttachmentViewModel, NSIndexPath) -> Void)?
     var didCompleteEditingAmount: (() -> Void)?

     init(claim: ClaimViewModel) {
          self.claim = claim
     }
     
     subscript (field: ClaimFormField) -> NSIndexPath {
          return NSIndexPath(forRow: basicFormFields.indexOf(field)!, inSection: basicFormIndex)
     }
     
     var sections: [ClaimFormSection] {
          var normalFields: [ClaimFormField] = [
               .Dependant,
               .Description,
               .Provider,
               .Amount,
               .ServiceDate
          ]
          
          if isInDatePickingMode {
               normalFields += [.DatePicker]
          }
          
          var results:[ClaimFormSection] = [
               .BasicInfo(fields: normalFields),
               .EOBs,
               .Receipts,
               .OtherAttachments
          ]
          
          if !claim.isNew {
               results += [.Delete]
          }
          
          return results
     }
     
     private var basicFormIndex: Int {
          return sections.indexOf { candidate in
               switch candidate {
               case .BasicInfo: return true
               default: return false
               }
          }!
     }

     private var basicFormFields: [ClaimFormField] {
          return sections.flatMap { (candidate) -> [ClaimFormField] in
               switch candidate {
               case let .BasicInfo(attachments): return attachments
               default: return []
               }
          }
     }
     
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
          return sections.count
     }
     
     func tableView(tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
          switch sections[sectionIndex] {
          case let .BasicInfo(fields): return fields.count
          case .EOBs: return claim.explanationOfBenefits.count + 1
          case .Receipts: return claim.receipts.count + 1
          case .OtherAttachments: return claim.otherAttachments.count + 1
          case .Delete: return 1
          }
     }
     
     func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {

          let section = sections[indexPath.section]
          switch section
          {
          case .EOBs: return indexPath.row < claim.explanationOfBenefits.count
          case .Receipts: return indexPath.row < claim.receipts.count
          case .OtherAttachments: return indexPath.row < claim.otherAttachments.count
          case .Delete: return false
          default: return false
          }
     }
     
     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
          if editingStyle == .Delete {
               let attachment = claim.attachmentForType(sections[indexPath.section])[indexPath.row]
               if let method = didRemoveItem { method(attachment, indexPath) }
          }
     }
     
     private func showDoneButtonOnKeyboard(claimAmountTextField : UITextField!)
     {
          let toolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
          toolbar.barStyle = UIBarStyle.Default
          
          let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
          let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(ClaimFieldsDataSource.doneButtonTapped))
          
          var items = [UIBarButtonItem]()
          items.append(flexSpace)
          items.append(done)
          
          toolbar.items = items
          toolbar.sizeToFit()
          claimAmountTextField.inputAccessoryView = toolbar
          
     }
     
     func doneButtonTapped(sender: AnyObject) {
          if let method = didCompleteEditingAmount { method() }
     }
     
     func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
          switch sections[section] {
          case .BasicInfo: return "Basic Details"
          case .EOBs: return "Explanation of Benefits"
          case .Receipts: return "Receipts"
          case .OtherAttachments: return "Other Attachments"
          default: return nil
          }
          
     }
     
     func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
          
          switch sections[section] {
          case .EOBs:
               return "If you have already received reimbursement for this claim from another insurance provider include their Explanation of Benefits"
          default:
               return nil
          }
     }
     
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
          let section = sections[indexPath.section]
          switch section {
          case let .BasicInfo(fields):
               
               switch fields[indexPath.row] {
               case .Dependant:
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("NewClaimCellWho", forIndexPath: indexPath) as! SingleLabelCell
                    cell.placeholder = "Who"
                    cell.disabled = disabled
                    cell.validationState = claim.validateDependant()
                    return cell
                    
               case .Description:
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("NewClaimCellWhat", forIndexPath: indexPath) as! SingleLabelCell
                    cell.placeholder = "What"
                    cell.disabled = disabled
                    cell.validationState = claim.validateDescription()
                    return cell
                    
               case .Provider:
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("NewClaimCellProvider", forIndexPath: indexPath) as! SingleLabelCell
                    cell.placeholder = "Where"
                    cell.disabled = disabled
                    cell.validationState = claim.validateProvider()
                    return cell
                    
               case .Amount:
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("NewClaimCellHowMuch", forIndexPath: indexPath) as! SingleTextFieldCell
                    showDoneButtonOnKeyboard(cell.textField)
                    cell.textField.text = claim.amountString
                    cell.disabled = disabled
                    cell.validationState = claim.validateAmount()
                    return cell
                    
               case .ServiceDate:
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("NewClaimCellWhen", forIndexPath: indexPath) as! SingleLabelCell
                    cell.placeholder = "When"
                    cell.disabled = disabled
                    cell.validationState = claim.validateServiceDate()
                    return cell
                    
               case .DatePicker:
                    
                    let cell = tableView.dequeueReusableCellWithIdentifier("NewClaimCellDatePicker", forIndexPath: indexPath) as! SingleDatePickerCell
                    cell.datePicker.maximumDate = NSDate()
                    cell.datePicker.minimumDate = NSDate().addMonth(-18)
                    if let serviceDate = claim.serviceDate {
                         cell.datePicker.date = serviceDate
                    }
                    return cell
                    
               }
          case .Delete:
               return tableView.dequeueReusableCellWithIdentifier("DeleteCell", forIndexPath: indexPath)
               
          default:
               
               let attachmentList = claim.attachmentForType(section)
               
               if indexPath.row == attachmentList.count {
                    
                    switch sections[indexPath.section] {
                    case .EOBs:
                         return tableView.dequeueReusableCellWithIdentifier("AddNewEOBCell", forIndexPath: indexPath)
                    case .Receipts:
                         return tableView.dequeueReusableCellWithIdentifier("AddNewReceiptCell", forIndexPath: indexPath)
                    case .OtherAttachments:
                         return tableView.dequeueReusableCellWithIdentifier("AddNewOtherAttachmentCell", forIndexPath: indexPath)
                    default: fatalError()
                    }
                    
               } else {
                    
                    let item = attachmentList[indexPath.row]
                    
                    let cell: NewClaimReceiptPhotoCell
                    
                    if item is PDFAttachmentViewModel {
                         cell = tableView.dequeueReusableCellWithIdentifier("PDFCell", forIndexPath: indexPath) as! NewClaimReceiptPhotoCell
                    } else if var vc = item as? ImageAttachmentViewModel {
                         cell = tableView.dequeueReusableCellWithIdentifier("NewClaimCellReceiptPhoto", forIndexPath: indexPath) as! NewClaimReceiptPhotoCell
                         
                         if let img = vc.image {
                              cell.photoImageView.image = img
                         } else if let id = vc.id {
                              
                              WebApi.sharedInstance.getAttachmentData(id, claimType: "OnlineClaim", completionBlock: { (data, response, error) in
                                   
                                   
                                   if (error != nil) {
                                        print(error)
                                   } else {
                                        let httpResponse = response as? NSHTTPURLResponse
                                        print(httpResponse)
                                        
                                        if let data = data
                                        {
                                             vc.image = UIImage(data: data)
                                             
                                             if httpResponse!.MIMEType == "image/png" {
                                                  
                                                  dispatch_async(dispatch_get_main_queue(),{
                                                       cell.photoImageView.image = vc.image
                                                  })
                                                  
                                             }
                                        }
                                   }
                                   
                              })
                              
                         }
                    } else {
                         fatalError()
                    }
                    
                    cell.attachmentId = item.id
                    
                    return cell
               }
          }
     }

}


class NewClaimViewController: UIViewController, SingleStringPickerDelegate,
     UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIPopoverPresentationControllerDelegate
{
     
     @IBOutlet weak var tableView: UITableView!
     @IBOutlet weak var errorMessageLabel: UILabel!
     @IBOutlet weak var errorView: UIView!
     @IBOutlet weak var submissionFailureBanner: UIView!
     
     var controller: UIImagePickerController?
     
     var originalClaim: Claim!
     var claim: ClaimViewModel!
     var dataSource: ClaimFieldsDataSource!
     
     var attachmentTypeToSave: ClaimFormSection?
     
     var errorMessage: String? {
          willSet {
               if let error = newValue {
                    errorMessageLabel.text = error
                    errorView.hidden = false
               } else {
                    errorView.hidden = true
               }
          }
     }
     
     var submissionFailure: Bool = false {
          willSet {
               if newValue {
                    submissionFailureBanner.hidden = false
                     self.navigationItem.rightBarButtonItem?.title = "Retry"
               } else {
                    submissionFailureBanner.hidden = true
                    self.navigationItem.rightBarButtonItem?.title = "Submit"
               }
          }
     }
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          tableView.registerNib(UINib(nibName: "NewClaimTableSectionHeaderView", bundle: nil),
                                forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
          
          
          tableView.registerNib(UINib(nibName: "NewClaimTableSectionHeaderViewForErrorMessage", bundle: nil),
                                forHeaderFooterViewReuseIdentifier: "TableSectionHeaderForErrorMessage")
          
          dataSource = ClaimFieldsDataSource(claim: claim)
          
          dataSource.didRemoveItem =  { [unowned self] c, ip in
               
               self.claim.removeAttachementForType(self.dataSource.sections[ip.section], atIndex: ip.row)
               
               self.tableView.deleteRowsAtIndexPaths([ip], withRowAnimation: .Fade)
               self.updateValidation()
          }
          dataSource.didCompleteEditingAmount = { [unowned self] in
               (self.tableView.cellForRowAtIndexPath(self.dataSource[.Amount]) as! SingleTextFieldCell).textField.resignFirstResponder()
          }
          
          tableView.dataSource = dataSource
          
          errorMessage = nil
          submissionFailure = false
          
          self.tableView.rowHeight = UITableViewAutomaticDimension
          self.tableView.estimatedRowHeight = 44
          
          
     }
     
     var showCalendar: Bool = false {
          willSet {
               
               if newValue {
                    if !showCalendar {
                         dataSource.isInDatePickingMode = newValue
                         let newIndexPath = dataSource[.DatePicker]
                         tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Automatic)
                    }
               } else {
                    if showCalendar {
                         let oldIndexPath = dataSource[.DatePicker]
                         dataSource.isInDatePickingMode = newValue
                         tableView.deleteRowsAtIndexPaths([oldIndexPath], withRowAnimation: .Automatic)
                    }
               }
          
          }
     }
     
     @IBAction func onAddPhoto(sender: AnyObject) {
          
          if let cell = sender as? UITableViewCell
               , indexPath = self.tableView.indexPathForCell(cell)
          {
               attachmentTypeToSave = dataSource.sections[indexPath.section]
               
               let ac = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
               
               ac.addAction(UIAlertAction(title: "Take Photo", style: .Default, handler: { (action) in
                    
                    self.onPopupCamera(self)
                    
               })
               )
               ac.addAction(UIAlertAction(title: "Use Last Photo Taken", style: .Default, handler: { (action) in
                    LastPhotoRetriever().queryLastPhoto(resizeTo: nil, queryCallback: { (image) in
                         // TODO: in what case can image be nil?
                         if let i = image { self.persistAttachment(i) } //TODO: why this has been called twice?
                    })
               })
               )
               ac.addAction(UIAlertAction(title: "Choose Photo", style: .Default, handler: { (action) in
                    
                    self.onPickNewImage(nil)
                    
               })
               )
               ac.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action) in
                    
               })
               )
               
               self.navigationController?.presentViewController(ac, animated: true, completion: nil)
               
          }
     }
     
     func showImageDetail(type: AttachmentType, receipt: AttachmentViewModel) {
          if let receipt = receipt as? PDFAttachmentViewModel {
           
               if let pdfData = receipt.pdf {
               
                    if let nvc = storyboard?.instantiateViewControllerWithIdentifier("WebViewNVC") as? UINavigationController,
                         webViewController =  nvc.viewControllers.first as? WebViewViewController
                    {
                         
                         //save data to tmp file so webviewer can display it
                         let filePath = "\(NSTemporaryDirectory())/attachment-\(type.rawValue)-\(receipt.id).pdf"
                         let filePathURL = NSURL(fileURLWithPath: filePath)
                         pdfData.writeToURL(filePathURL, atomically: true)
                         
                         webViewController.title = type.singularDetail
                         webViewController.pdfUrl = filePathURL
                              
                         presentViewController(nvc, animated: true, completion: nil)
                    }
               }
               
          } else if let receipt = receipt as? ImageAttachmentViewModel {
               
               var photo:IDMPhoto = IDMPhoto()
               
               if let imageData = receipt.image {
                    photo = IDMPhoto(image: imageData)
               } else {
                    fatalError("No image to display")
               }
               
               photo.caption = type.singularDetail
               
               let photosViewController = IDMPhotoBrowser(
                    photos:
                    [photo]
               )
               photosViewController.displayActionButton = false
               
               presentViewController(photosViewController, animated: true, completion: nil)
          }
     }
     
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
     {
          guard !dataSource.disabled else {
               return
          }
          
          tableView.deselectRowAtIndexPath(indexPath, animated: false)
          
          switch dataSource.sections[indexPath.section] {
          case let .BasicInfo(list) where list[indexPath.row] == .ServiceDate:
               if self.claim.serviceDate == nil
               {
                    // default service date to today.
                    self.claim.serviceDate = NSDate()
                    updateValidation()
               }
               
               showCalendar = !showCalendar
               
          case .BasicInfo:
               showCalendar = false
          case .EOBs:
               if indexPath.row < claim.explanationOfBenefits.count {
                    showImageDetail(.EOB, receipt: claim.explanationOfBenefits[indexPath.row])
               } else {
                    onAddPhoto(tableView.cellForRowAtIndexPath(indexPath)!)
               }
               showCalendar = false
          case .Receipts:
               if indexPath.row < claim.receipts.count {
                    showImageDetail(.Receipt, receipt: claim.receipts[indexPath.row])
               } else {
                    onAddPhoto(tableView.cellForRowAtIndexPath(indexPath)!)
               }
               showCalendar = false
          case .OtherAttachments:
               if indexPath.row < claim.otherAttachments.count {
                    showImageDetail(.Other, receipt: claim.otherAttachments[indexPath.row])
               } else {
                    onAddPhoto(tableView.cellForRowAtIndexPath(indexPath)!)
               }
               showCalendar = false
               
          case .Delete(_):
               
               showCalendar = false
          }
     }
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
          // Get the new view controller using segue.destinationViewController.
          // Pass the selected object to the new view controller.
          
          if segue.identifier == "NewClaimToProviderPicker" {
               if let viewController = segue.destinationViewController as? StringPickerViewController {
                    viewController.delegate = self
                    
                    //                viewController.acceptSearchTextAsPickResult = true
                    viewController.title = "Providers"
                    viewController.senderTag = PickerSenderTag.Provider.rawValue
                    
                    
                    
               }
          }else if segue.identifier == "NewClaimToServiceDescriptionPicker" {
               if let viewController = segue.destinationViewController as? ServiceDescriptionPickerViewController {
                    viewController.delegate = self
                    
                    
                    viewController.title = "Service Descriptions"
                    viewController.senderTag = PickerSenderTag.ServiceDescription.rawValue
                    
                    
               }
          }else if segue.identifier == "NewClaimToPatientPicker" {
               if let viewController = segue.destinationViewController as? StringPickerViewController {
                    viewController.delegate = self
                    
                    viewController.title = "Patients"
                    viewController.senderTag = PickerSenderTag.Patient.rawValue
                    viewController.allowAddCustomInputItemAsResult = false
                    viewController.hideSearchBar = true
                    
                    if let member = Member.currentMember {
                         
                         viewController.items = [member.fullName!]
                         
                         if let dependants = member.dependants
                         {
                              let list =  dependants.map({ (dependant) -> String in
                                   dependant.fullName as String!
                              })
                              
                              viewController.items.appendContentsOf(list)
                              
                         }
                         
                         viewController.reloadData()
                         
                    }
               }
          }
          
     }
     
     // MARK: - SingleStringPickerDelegate
     func onResultPicked(result: String, senderTag: NSInteger, resultId: String?) {
          //        let indexPath = NSIndexPath(forRow: senderTag, inSection: 0)
          
          switch PickerSenderTag(rawValue: senderTag)! {
          case .Patient:
               
               // TODO: This logic needs to change

               let id: String?
               
               if result == Member.currentMember?.fullName {
                    id = Member.currentMember?.id
               } else if let recipientAsebpId = Member.currentMember?.dependants?.filter({ $0.fullName == result }).first?.id {
                    id = recipientAsebpId
               } else {
                    id = nil
               }
               
               self.claim.recipient = RecipientViewModel(id: id, name: result)

          case .ServiceDescription:
               self.claim.serviceDescription = result
          case .Provider:
               self.claim.providerName = result
          default:
               fatalError()
          }
          
          updateValidation()
     }
     
     
     // MARK: - DatePicker
     @IBAction func onDatePicked(picker: UIDatePicker)
     {
        self.claim.serviceDate = picker.date
        updateValidation()
     }
     
     /*
      // MARK - UIImagePicker
      */
     func imagePickerController(picker: UIImagePickerController,
                                didFinishPickingMediaWithInfo info: [String: AnyObject]){
          
          print("Picker returned successfully")
          
          let mediaType:AnyObject? = info[UIImagePickerControllerMediaType]
          
          if let type:AnyObject = mediaType{
               
               if type is String{
                    let stringType = type as! String
                    
                    if stringType == kUTTypeMovie as String{
                         let urlOfVideo = info[UIImagePickerControllerMediaURL] as? NSURL
                         if let url = urlOfVideo{
                              print("Video URL = \(url)")
                         }
                    }
                         
                    else if stringType == kUTTypeImage as String{
                         /* Let's get the metadata. This is only for images. Not videos */
                         //                        let metadata = info[UIImagePickerControllerMediaMetadata]
                         //                            as? NSDictionary
                         //                        if let theMetaData = metadata{
                         let image = info[UIImagePickerControllerEditedImage]
                              as? UIImage
                         if let theImage = image{
                              picker.dismissViewControllerAnimated(true) {
                                   self.persistAttachment(theImage)
                              }
                              
                              
                              
                         }
                    }
                    
               }
          }
          
          picker.dismissViewControllerAnimated(true, completion: nil)
     }
     
     func persistAttachment(image: UIImage) {
          
          claim.addAttachmentForType(attachmentTypeToSave!, attachment: ImageAttachmentViewModel(image: image))
          
          tableView.reloadData()
          updateValidation()
          
          self.attachmentTypeToSave = nil

     }
     
     func imagePickerControllerDidCancel(picker: UIImagePickerController) {
          print("Picker was cancelled")
          picker.dismissViewControllerAnimated(true, completion: nil)
     }
     
     func isCameraAvailable() -> Bool{
          return UIImagePickerController.isSourceTypeAvailable(.Camera)
     }
     
     func cameraSupportsMedia(mediaType: String,
                              sourceType: UIImagePickerControllerSourceType) -> Bool{
          
          let availableMediaTypes =
               UIImagePickerController.availableMediaTypesForSourceType(sourceType) as
                    [String]?
          
          if let types = availableMediaTypes{
               for type in types{
                    if type == mediaType{
                         return true
                    }
               }
          }
          
          return false
     }
     
     func doesCameraSupportTakingPhotos() -> Bool{
          return cameraSupportsMedia(kUTTypeImage as String, sourceType: .Camera)
     }
     
     func onPopupCamera(sender: AnyObject)
     {
          if !isCameraAvailable() {
               return
          }
          
          controller = UIImagePickerController()
          
          if let theController = controller{
               theController.sourceType = .Camera
               theController.cameraDevice = .Rear
               
               theController.mediaTypes = [kUTTypeImage as String]
               
               theController.allowsEditing = true
               theController.delegate = self
               
               presentViewController(theController, animated: true, completion: nil)
          }
     }
     
     @IBAction func onPickNewImage(sender : UIButton?) {
          
          controller = UIImagePickerController()
          
          if let theController = controller{
               theController.sourceType = .PhotoLibrary
               
               theController.mediaTypes = [kUTTypeImage as String]
               
               theController.allowsEditing = true
               theController.delegate = self
               
               presentViewController(theController, animated: true, completion: nil)
          }
          
          
     }
     
     func attachmentChanges() -> (addedAttachments: [Attachment], removedAttachments: [Attachment]) {
          return ([], [])
     }
     
     func finalizeSubmission(claimId: String) -> Observable<Void> {
          return WebApi.sharedInstance.finalizePost(claimId)
     }
     
     func uploadAttachments(claimId: String) -> Observable<String> {
          
          let (addedAttachments, removedAttachments) = attachmentChanges()
          
          let adds = addedAttachments.map { WebApi.sharedInstance.addAttachment(claimId, attachment: $0) }
          let removals = removedAttachments.map { WebApi.sharedInstance.removeAttachment(claimId, attachment: $0) }
          
          let all = (adds + removals)
          
          if all.count > 0 {
               return all.zip { _ in claimId }
          } else {
               return Observable.just(claimId)
          }

     }
     
     let db = DisposeBag()
     
     @IBAction func onCancel(sender: AnyObject) {
          view.endEditing(true)
          cancel()
     }
     
     @IBAction func onSubmit()
     {
          dataSource.disabled = true
          dataSource.isInDatePickingMode = false

          submissionFailure = false
          
          let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
          self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
          activityIndicator.startAnimating()
          
          WebApi.sharedInstance.submitPost(claim.asClaim())
               .flatMap(uploadAttachments)
               .flatMap(finalizeSubmission)
               .subscribe {
                    switch $0 {
                    case .Error:
                         self.dataSource.disabled = false
                         self.dataSource.isInDatePickingMode = true
                         self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Retry", style: .Plain, target: self, action: #selector(self.onSubmit))
                         self.submissionFailure = true
                    case .Next:
                         self.submit()
                    default:
                         break
                    }
               }
               .addDisposableTo(db)

     }
     
     
     func updateValidation() {
          let validationResults = claim.validateAll()
          
          validationResults.forEach { result in
               if let fieldResult = result as? FieldValidationResult {
                    if var cell = self.tableView.cellForRowAtIndexPath(self.dataSource[fieldResult.field]) as? Validatable
                    {
                         cell.validationState = fieldResult.status
                    }else{
                         NSLog("ingore non validatable field \(fieldResult.field)")
                    }
               }
          }
          
          let errors = validationResults.flatMap { (result) -> String? in
               if let fieldResult = result as? FieldValidationResult {
                    switch fieldResult.status {
                    case let .Error(_, message): return message
                    default: return nil
                    }
               } else {
                    return nil
               }
          }
          
          self.errorMessage = errors.first
          
          let errorsOrMissing = validationResults.filter { result in
               switch result.status {
               case .Error, .Empty: return true
               default: return false
               }
          }
          
          self.navigationItem.rightBarButtonItem?.enabled = errorsOrMissing.count == 0
     }
     
     func validateClaimData()
     {
          
          
          WebApi.sharedInstance.validateClaimSubmission(Member.currentMember!.id!,
                                                        memberId: Member.selectedMembershipId!,
                                                        claim: self.claim.asClaim()
          ) { (results, error) in
               
               if let results = results
               {
                    self.claim.validationErrors = results
                    self.updateValidation()
               }
          }
     }
     
     var amountTypedString:String = ""
     
     // MARK: - UITextFieldDelegate
     func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
          self.showCalendar = false
          
          if textField.text == nil || textField.text!.characters.count == 0 {
               textField.text = "0.00"
          }

          return true
     }
     
     func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
          
          let formatter = NSNumberFormatter()
          formatter.minimumFractionDigits = 2
          formatter.maximumFractionDigits = 2

          //Must be backspace ("") or a digit between 0 and 9
          guard string == "" || (string >= "0" && string <= "9") else { return false }
          
          if string.characters.count > 0 {
               
               amountTypedString += string
               let decNumber =  NSDecimalNumber(string:  (amountTypedString)).decimalNumberByMultiplyingBy(0.01)
               let newString = (decNumber.doubleValue < 1.0 ? "0" : "") + formatter.stringFromNumber(decNumber)!
               textField.text = newString
               
          } else {
               
               amountTypedString = String(amountTypedString.characters.dropLast())
               if amountTypedString.characters.count > 0 {
                    let decNumber = NSDecimalNumber(string: (amountTypedString)).decimalNumberByMultiplyingBy(0.01)
                    let newString = (decNumber.doubleValue < 1.0 ? "0" : "") +  formatter.stringFromNumber(decNumber)!
                    textField.text = newString
               } else {
                    textField.text = "0.00"
               }
               
          }
          
          self.claim.amount = NSDecimalNumber(double:Double( textField.text! )!)
          updateValidation()
          
          return false
          
     }
     
     // MARK: - UIPopoverPresentationControllerDelegate 
     
     func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
          return UIModalPresentationStyle.None
     }
     
     func presentationController(controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
          let navigationController = UINavigationController(rootViewController: controller.presentedViewController)
          let btnDone = UIBarButtonItem(title: "Done", style: .Done, target: self, action: #selector(submit))
          navigationController.topViewController!.navigationItem.rightBarButtonItem = btnDone
          return navigationController
     }
     
     func submit() {
          self.performSegueWithIdentifier("submit", sender: self)
     }
    
    func cancel() {
        self.performSegueWithIdentifier("cancel", sender: self)
    }
}
