//
//  WalletCardViewController.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-11.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import SwiftyJSON

class WalletCardViewController: UITableViewController {
    
    @IBOutlet weak var memberFullNameLabel: UILabel!
    @IBOutlet weak var memberIDLabel: UILabel!
    @IBOutlet weak var memberGroupSectionLabel: UILabel!
    @IBOutlet weak var memberIssedDateLabel: UILabel!
    var dependants:[Dependant] = []
    var benefits:[Benefit] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.tableView.estimatedRowHeight = 20
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        
        let nib1 = UINib(nibName: "WalletCardDependantSectionHeaderView", bundle: nil)
        tableView.registerNib(nib1, forHeaderFooterViewReuseIdentifier: "TableSectionHeader1")

        let nib2 = UINib(nibName: "WalletCardBenefitSectionHeaderView", bundle: nil)
        tableView.registerNib(nib2, forHeaderFooterViewReuseIdentifier: "TableSectionHeader2")
        
        loadData()

    }
    
    func loadData()
    {
        if let member = Member.currentMember,
            selectedMembershipId = Member.selectedMembershipId
        {
 
            var currentMembership = member.memberships?.filter({ (membership) -> Bool in
                membership.id == selectedMembershipId
            }).first
            
            if currentMembership == nil
            {
                currentMembership = member.memberships!.first
            }
            
            self.memberIDLabel.text = member.id
            self.memberFullNameLabel.text = member.fullName
            self.memberGroupSectionLabel.text = "\(member.groupCode!) / \(currentMembership!.sectionCode!)"
            self.memberIssedDateLabel.text = member.issuedDate?.shortDateFormat()
            
            self.benefits = currentMembership!.benefits ?? []
            
            self.dependants = member.dependants ?? []
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onDone(sender: AnyObject) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return dependants.count
        case 1:
            return benefits.count
        default:
            return 0
        }
        
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return [30.0, 40.0][section]
    }

    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.tableView.dequeueReusableHeaderFooterViewWithIdentifier( ["TableSectionHeader1", "TableSectionHeader2"][section])
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCellWithIdentifier("WalletCardDependantCell", forIndexPath: indexPath) as? WalletCardDependantCell
            {
                let item = dependants[indexPath.row]
                
                cell.idLabel.text = item.id
                cell.nameLabel.text = item.fullName
                
                return cell

                
            }
            
        case 1:
            if let cell = tableView.dequeueReusableCellWithIdentifier("WalletCardBenefitCell", forIndexPath: indexPath) as? WalletCardBenefitCell
            {
                let item = benefits[indexPath.row]
                
                cell.classLabel.text = item.coverageLevel
                cell.nameLabel.text = item.name
                
                return cell
                
                
            }
        
            
        default:
            return UITableViewCell()
        }
        
         return UITableViewCell()
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
