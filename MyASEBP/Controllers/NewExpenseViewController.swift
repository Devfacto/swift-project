//
//  NewExpenseViewController.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-11.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewExpenseViewController: UIViewController, SingleStringPickerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {
    
    
    //TODO: Refactor receipts approach to new claim model approach for tracking additions and removals
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var constraintErrorMessageLabelTop: NSLayoutConstraint!
    @IBOutlet weak var constraintErrorMessageLabelBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintErrorMessageContainerViewHeightZero: NSLayoutConstraint!
   
    
    var isInDatePickingMode = false
    var receiptPhotos: [AttachmentViewModel] = []
    var expenseAmountTextField:UITextField!
    var validationErrors: [String: AnyObject] = Dictionary()
    var claim:Claim!
    var editingFieldName: String?
    var amountTypedString:String = ""

    var errorKeyNames:[Int: String] = [
        1 : "description",
        3 : "amount",
        4 : "serviceDate"]
    
    enum ErrorKeyName : String {
        case Description = "description"
        case Amount = "amount"
        case ServiceDate = "serviceDate"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "NewClaimTableSectionHeaderView", bundle: nil)
        
        tableView.registerNib(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        tableView.registerNib(UINib(nibName: "NewClaimTableSectionHeaderViewForErrorMessage", bundle: nil),
                              forHeaderFooterViewReuseIdentifier: "TableSectionHeaderForErrorMessage")
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
        
        loadData(true)
        // Do any additional setup after loading the view.
        switch claim.type {
        case .HSA: self.title = "HSA"
        case .WSA: self.title = "WSA"
        default: fatalError()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
      
        self.tableView.reloadData()
        self.displayError()
        self.validateClaimData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onDone(sender: AnyObject) {
        view.endEditing(true)
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onCancel(sender: AnyObject) {
        view.endEditing(true)
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onDatePicked(picker : UIDatePicker) {
        
        self.claim.serviceDate = picker.date
        
        self.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 1, inSection: 1)], withRowAnimation: .Automatic)
        
    }
    
    @IBAction func onEditExpenseAmount(sender: AnyObject) {
        
        self.tableView.deselectRowAtIndexPath(NSIndexPath(forRow: 5, inSection: 1), animated: false)
        let calendarCellIndexPath = NSIndexPath(forRow: 5, inSection: 1)
        
        if self.isInDatePickingMode {
            self.isInDatePickingMode = false
            self.tableView.deleteRowsAtIndexPaths([calendarCellIndexPath], withRowAnimation: .Automatic)
            
            let dateCellIndexPath = NSIndexPath(forRow: 4, inSection: 1)
            self.tableView.reloadRowsAtIndexPaths([dateCellIndexPath], withRowAnimation: .Automatic)
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        switch claim.type {
        case .HSA: return 2
        case .WSA: return 3
        default: fatalError()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return isInDatePickingMode ? 6 : 5
        case 2:
            return receiptPhotos.count
        default:
            return Member.selectedMembership!.coveragePeriods.count
        }
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section > 0 {
            return 40.0
        }
        
        if self.validationErrors.keys.count == 0
        {
            return 40.0
        }
        
        let identifier = section == 0 ? "TableSectionHeaderForErrorMessage" : "TableSectionHeader"
        let cell = self.tableView.dequeueReusableHeaderFooterViewWithIdentifier(identifier)
        let header = cell as! TableSectionHeader
        
        
        
        if let editingFieldName = editingFieldName
        {
            if self.validationErrors.keys.contains(editingFieldName)
            {
                let labelHeight = heightForView(self.validationErrors[editingFieldName] as! String, labelToSet: header.titleLabel)
                
                return labelHeight + 24
            }
        }
        
        return 40.0
    }
    
    func heightForView(text:String, labelToSet: UILabel) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, labelToSet.frame.size.width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = labelToSet.font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let title = ["Account Balance","Basic Details", "Receipts"][section]
        let cell = self.tableView.dequeueReusableHeaderFooterViewWithIdentifier("TableSectionHeader")
        let header = cell as! TableSectionHeader
        header.titleLabel.text = title.uppercaseString
        
        if section == 1 {
            header.addPhotoButton.hidden = true
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var identifiers = ["NewExpenseCellWho",
                           "NewExpenseCellWhat",
                           "NewExpenseCellDescription",
                           "NewExpenseCellHowMuch",
                           "NewExpenseCellWhen"]
        
        if isInDatePickingMode {
            identifiers.append("NewExpenseCellDatePicker")
        }
        
        if indexPath.section == 0 {
            
            //TODO: Avoid explicitly unwrapping optional
            let coveragePeriod = Member.selectedMembership!.coveragePeriods[indexPath.row]
            
            var identifier = "AccountCell"
            
            if !coveragePeriod.allocationSet {
                identifier = "AccountCellNoAllocation"
            }
            
            
            let cell = tableView.dequeueReusableCellWithIdentifier(
                identifier, forIndexPath: indexPath) as! AccountCell
            
            switch claim.type {
            case .HSA:
                cell.showHsaBalance = true
                cell.showWsaBalance = false
            case .WSA:
                cell.showHsaBalance = false
                cell.showWsaBalance = true
            default: fatalError()
            }
            
            cell.hideAccountTypeLabel = true
            cell.coveragePeriod = coveragePeriod
            
            
            return cell
        }
        
        if indexPath.section == 1 {
            let identifier = identifiers[indexPath.row]
            let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath)
            
            if let cell = cell as? SingleLabelCell
            {
                
                cell.titleLabel.textColor = UIColor.placeholderGrayColor()
                
                if let claim = self.claim {
                    switch indexPath.row {
                    case 0:
                        if let recipientName = claim.recipientName
                        {
                            cell.titleLabel.text = recipientName
                            cell.titleLabel.textColor = UIColor.blackColor()
                            
                            
                        }
           
                    case 1:
                        if let description = claim.serviceDescription
                        {
                            cell.titleLabel.text = description
                            cell.titleLabel.textColor = UIColor.blackColor()
                        }
                    case 4:
                        if let serviceDate = claim.serviceDate
                        {
                            cell.titleLabel.text = serviceDate.shortDateFormat()
                            if self.isInDatePickingMode {
                                cell.titleLabel.textColor = UIColor.redColor()
                            }else{
                                cell.titleLabel.textColor = UIColor.blackColor()
                            }
                        }
                        
                    default:
                        break
                    }
                    
                }
                else{
                    
                    cell.titleLabel.textColor = UIColor.placeholderGrayColor()
                    
                }
                
                return cell
            }
            
            if let cell = cell as? SingleTextFieldCell{
                if identifier == "NewExpenseCellHowMuch" {
                    expenseAmountTextField = cell.textField
                    expenseAmountTextField.text = claim?.amount?.doubleValue.format(".02")
                    showDoneButtonOnKeyboard(expenseAmountTextField)
                    
                    if let imageView = cell.viewWithTag(1) as? UIImageView {
                        
                        imageView.image = UIImage(named: "How Much Icon Grey")
                        
                        if editingFieldName == ErrorKeyName.Amount.rawValue
                            &&  self.validationErrors.keys.contains(ErrorKeyName.Amount.rawValue)
                        {
                            imageView.image = UIImage(named: "How Much Icon Red")
                            
                            cell.textField.textColor = UIColor.redColor()
                            cell.textField.attributedPlaceholder = NSAttributedString(string: "How Much?", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
                            
                        }
                        
                    }

                }
                
                cell.textField.addTarget(self, action: #selector(NewExpenseViewController.onEditExpenseAmount(_:)), forControlEvents: .EditingDidBegin)
                return cell
                
            }
            
            if indexPath.row == 4 && isInDatePickingMode {
                if let cell = cell as? SingleDatePickerCell
                {
                    cell.datePicker.maximumDate = NSDate()
                    cell.datePicker.minimumDate = NSDate().addMonth(-18)
                    if let claim = self.claim , serviceDate = claim.serviceDate {
                        cell.datePicker.date = serviceDate
                    }
                }
            }
            
            return cell
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        if indexPath.section == 1 && indexPath.row == 4{
            self.isInDatePickingMode = !self.isInDatePickingMode
            
        }else{
            
            if self.isInDatePickingMode {
                self.isInDatePickingMode = false
            }else{
                
                // no animation needed
                return
                
            }
        }
        
        let calendarCellIndexPath = NSIndexPath(forRow: 5, inSection: 1)
        let dateCellIndexPath = NSIndexPath(forRow: 4, inSection: 1)
        
        if isInDatePickingMode
        {
            self.tableView.insertRowsAtIndexPaths([calendarCellIndexPath], withRowAnimation: .Automatic)
            
            self.tableView.scrollToRowAtIndexPath(calendarCellIndexPath, atScrollPosition: .Middle, animated: true)
            
        }else{
            
            self.tableView.deleteRowsAtIndexPaths([calendarCellIndexPath], withRowAnimation: .Automatic)
        }
        
        self.tableView.reloadRowsAtIndexPaths([dateCellIndexPath], withRowAnimation: .Automatic)
         self.validateClaimData()
        
    }
    
    
    func showDoneButtonOnKeyboard(expenseAmountTextField : UITextField!)
    {
        let toolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, 320, 50))
        toolbar.barStyle = UIBarStyle.Default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(NewExpenseViewController.doneButtonTapped))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        toolbar.items = items
        toolbar.sizeToFit()
        expenseAmountTextField.inputAccessoryView = toolbar
        
    }
    
    func doneButtonTapped()
    {
        self.expenseAmountTextField.resignFirstResponder()
    }
    
    func loadData(showHUD: Bool = false)
    {
        if showHUD {
            SVProgressHUD.show()
        }
        
        let group = dispatch_group_create()
        
        // pair a dispatch_group_enter for each dispatch_group_leave
        dispatch_group_enter(group)
        
        // TODO: remove this unnecessory member ship refresh
        WebApi.sharedInstance.getMemberInfo(Member.currentMember!.id!, completionBlock: { (member, error) in
            
            dispatch_group_leave(group)
            
            if let error =  error
            {
                NSLog(error.localizedDescription)
                
                let ac = UIAlertController(title: "Error",
                    message: error.localizedDescription,
                    preferredStyle: .Alert)
                
                ac.addAction(UIAlertAction(title: "Ok",
                    style: .Default,
                    handler: nil))
                
                self.presentViewController(ac, animated: true, completion: nil)
            }
            
            if let member = member
            {
                Member.currentMember = member
                
                
                
            }
            
        })
        
        
        dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            dispatch_async(dispatch_get_main_queue()) {
                
                
                if showHUD {
                    SVProgressHUD.dismiss()
                }
                
                self.tableView.reloadData()
            }
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "NewExpenseToPatientPicker" {
            if let viewController = segue.destinationViewController as? StringPickerViewController {
                viewController.delegate = self
                
                viewController.title = "Patients"
                viewController.senderTag = PickerSenderTag.Patient.rawValue
                viewController.allowAddCustomInputItemAsResult = true
                viewController.hideSearchBar = false
                
                if let member = Member.currentMember {
                    
                    viewController.items = [member.fullName!]
                    
                    if let dependants = member.dependants
                    {
                        let list =  dependants.map({ (dependant) -> String in
                            dependant.fullName as String!
                        })
                        
                        viewController.items.appendContentsOf(list)
                        
                    }
                    
                    viewController.reloadData()
                    
                }
            }
        }else if segue.identifier == "NewExpenseToCategoryPicker"{
            if let viewController = segue.destinationViewController as? ExpenseCategoryPickerViewController
            {
                viewController.delegate = self
                
                
                viewController.title = "Expense Category"
                
                switch claim.type {
                case .HSA: viewController.accountType = "HSA"
                case .WSA: viewController.accountType = "WSA"
                default: fatalError()
                }
                viewController.senderTag = PickerSenderTag.ExpenseCategory.rawValue
            }
        }
    }
    
    // MARK: - Implement SingleStringPickerDelegate
    func onResultPicked(result: String, senderTag: NSInteger, resultId: String?) {
        
        switch senderTag {
        case 0:
            self.claim?.recipientName = result
            
            if result == Member.currentMember?.fullName
            {
                self.claim?.recipientAsebpId = Member.currentMember?.id
            }else if let recipientAsebpId = Member.currentMember?.dependants?.filter({ (dependant) -> Bool in
                dependant.fullName == result
            }).first?.id
            {
                self.claim?.recipientAsebpId = recipientAsebpId
            }else{
                self.claim?.recipientAsebpId = ""
            }
        case 1:
            self.claim?.serviceDescription = result
            
            NSLog("category id \(resultId)")
            
            
        default:
            break
        }
        
        editingFieldName = errorKeyNames[senderTag]
        
        validateClaimData()
        
    }
    
 
    func validateClaimData()
    {
        self.navigationItem.rightBarButtonItem?.enabled = false
        //self.displayError()
        
        if editingFieldName == nil {
            return
        }

        WebApi.sharedInstance.validateClaimSubmission(Member.currentMember!.id!,
                                                      memberId: Member.selectedMembershipId!,
                                                      claim: self.claim!
        ) { (results, error) in
            
            if let results = results
            {
                if results.keys.count == 0
                {
                    // no validation errors
                    
                    self.navigationItem.rightBarButtonItem?.enabled = true
                }else{
                    self.navigationItem.rightBarButtonItem?.enabled = false
                    
                    
                }
                
                self.validationErrors = results
                
                self.tableView.reloadData()
            }
            
            self.displayError()
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if textField.text?.characters.count == 0
        {
            textField.text = "0.00"
            textField.textColor = UIColor.redColor()
        }else{
            textField.textColor = UIColor.blueColor()
        }
        
        
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        let formatter = NSNumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        if string.characters.count > 0 {
            
            if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 3, inSection: 0)),
                
            imageView = cell.viewWithTag(1) as? UIImageView
            {
                imageView.image = UIImage(named: "How Much Icon Grey")
            }
            
            textField.textColor = UIColor.blueColor()
            
            amountTypedString += string
            let decNumber =  NSDecimalNumber(string:  (amountTypedString)).decimalNumberByMultiplyingBy(0.01)
            let newString = (decNumber.doubleValue < 1.0 ? "0" : "") + formatter.stringFromNumber(decNumber)!
            textField.text = newString
        } else {
            amountTypedString = String(amountTypedString.characters.dropLast())
            if amountTypedString.characters.count > 0 {
                let decNumber = NSDecimalNumber(string: (amountTypedString)).decimalNumberByMultiplyingBy(0.01)
                let newString = (decNumber.doubleValue < 1.0 ? "0" : "") +  formatter.stringFromNumber(decNumber)!
                textField.text = newString
            } else {
                textField.text = "0.00"
            }
            
        }
        
        
        return false
        
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        self.claim?.amount = NSDecimalNumber(double:Double( textField.text! )!)
        
        editingFieldName = ErrorKeyName.Amount.rawValue

        validateClaimData()
    }
    
    func displayError()
    {
        self.errorMessageLabel.text = ""
        constraintErrorMessageLabelTop.constant = 0
        constraintErrorMessageLabelBottom.constant = 0
        
        if let editingFieldName = editingFieldName
        {
            if self.validationErrors.keys.contains(editingFieldName)
            {
                self.errorMessageLabel.text = self.validationErrors[editingFieldName] as? String
              
                constraintErrorMessageLabelTop.constant = 8
                constraintErrorMessageLabelBottom.constant = 8
                
            }else{
          
            }
        }
        
    }

    

}



