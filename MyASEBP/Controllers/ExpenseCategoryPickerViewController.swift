//
//  ProviderPickerViewController.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-31.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class ExpenseCategoryPickerViewController: StringPickerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        allowAddCustomInputItemAsResult = false
        
        self.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var categoryList:[ExpenseCategory] = []

    var accountType: String!

    override func reloadData() {
        

        WebApi.sharedInstance.getExpenseCategoryList(Member.currentMember!.id!, accountType: accountType) { (results, error) in
            
            
            if  let results = results {
                
                self.categoryList = results
 
                self.items = results.map({ (result) -> String in
 
                    return "\(result.categoryName) - \(result.name)"
                    
                })
                
                self.tableView.reloadData()
             
                
            }
            
            if let error = error {
                
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                
                self.presentViewController(alert, animated: true, completion: nil)
                
            }
            // TODO: handle error
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override var filteredItems:[String] {
        get{
            if let stringToSearch = (self.searchBar != nil) ? self.searchBar!.text : ""
            {
                if stringToSearch.characters.count > 0
                {
                    return items.filter({ (item) -> Bool in
                        return item.localizedCaseInsensitiveContainsString(stringToSearch)
                    })
                }
                
            }
            
            return items
            
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var result = self.searchBar != nil ? self.searchBar!.text : ""
        if (filteredItems.count > indexPath.row) {
            result = filteredItems[indexPath.row]
        }
        
        
 
        
        self.delegate.onResultPicked(result!, senderTag: self.senderTag , resultId: self.categoryList[indexPath.row].id)
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
 
}
