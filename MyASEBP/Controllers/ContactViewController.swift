//
//  ContactViewController.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-17.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class ContactViewController: UITableViewController {
    
    @IBOutlet weak var contactPhoneNumberLabel: UILabel!
    
    @IBOutlet weak var contactEmailLabel: UILabel!
    
    @IBOutlet weak var contactAddressLabel: UILabel!
    
    @IBOutlet weak var hoursOpertationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactPhoneNumberLabel.text = Client.ContactPhoneNumber
        contactEmailLabel.text = Client.ContactEmail
        contactAddressLabel.text = Client.ContactAddress
        hoursOpertationLabel.text = Client.HoursOperation
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - ()
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        switch indexPath.section {
        case 0:
            
            let ac = UIAlertController(title: "Call",
                                       message: "\(Client.ContactPhoneNumber)",
                                       preferredStyle: .Alert)
            
            ac.addAction(UIAlertAction(title: "Ok",
                style: .Default,
                handler: { (_) in
                    let url = NSURL(string: "tel://\(Client.ContactPhoneNumber)")!
                    UIApplication.sharedApplication().openURL(url)
            }))
            
            ac.addAction(UIAlertAction(title: "Cancel",
                style: .Cancel,
                handler: nil))
            
            self.navigationController?.presentViewController(ac, animated: true, completion: nil)
            
            
        case 1:
            let url = NSURL(string: "mailto:" + Client.ContactEmail)!
            UIApplication.sharedApplication().openURL(url)
        case 2:
            let address = Client.ContactAddress.stringByReplacingOccurrencesOfString("\n", withString: "").stringByReplacingOccurrencesOfString(" ", withString: "+")
           
            let url = NSURL(string: Client.ContactMapUrl.stringByAppendingString(address))!
            UIApplication.sharedApplication().openURL(url)
            
        default:
            print("do nothing")
            
        }
        
    }
 
    
}
