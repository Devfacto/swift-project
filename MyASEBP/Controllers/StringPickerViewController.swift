//
//  ProviderPickerViewController.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-18.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol SingleStringPickerDelegate {
    
    func onResultPicked(result: String, senderTag: NSInteger, resultId: String?)
    
}


class StringPickerViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar?
    
    
    var hideSearchBar: Bool = false
    var allowAddCustomInputItemAsResult: Bool = true
    
    var delegate:SingleStringPickerDelegate! = nil
    
    var items:[String] = []
    
    var senderTag:NSInteger = 0
    
    var filteredItems:[String] {
        get{
            if let stringToSearch = (self.searchBar != nil) ? self.searchBar!.text : ""
            {
                if stringToSearch.characters.count > 0
                {
                    return items.filter({ (item) -> Bool in
                        return item.localizedCaseInsensitiveContainsString(stringToSearch)
                    })
                }
                
            }
            
            return items
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if allowAddCustomInputItemAsResult {
            hideSearchBar = false
        }
     
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
        
        
        if hideSearchBar {
            self.tableView.tableHeaderView = nil
        }else{
            searchBar?.becomeFirstResponder()
        }
        
    }
    
    func reloadData()
    {
  
        self.tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UISearchBarDelegate
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
 
    }
    
    func shouldBeAbleToAddSearchTextAsNewItem() -> Bool
    {
        if let searchBar = self.searchBar,
            searchText = searchBar.text
        {
            
            if searchText.characters.count > 2 {
                return true
            }
            
        }
        return false
    }
    
    // MARK: - ()
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !allowAddCustomInputItemAsResult {
            return self.filteredItems.count
        }
        
        return
 
                (  self.shouldBeAbleToAddSearchTextAsNewItem() ? self.filteredItems.count + 1 : self.filteredItems.count)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (self.filteredItems.count > indexPath.row) {
            let cell = tableView.dequeueReusableCellWithIdentifier("ProviderCell", forIndexPath: indexPath)

            let item = filteredItems[indexPath.row]
           
            cell.textLabel!.numberOfLines = 0
            cell.textLabel!.text = item
            cell.detailTextLabel!.text = "" //TODO: show birth date for depandants
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StringPickerAddNewCell", forIndexPath: indexPath)
        if let cell  = cell as? StringPickerAddNewCell
        {
        
            cell.addNewLabel.text = "Add \"\(self.searchBar!.text!.trimSpaces())\""
            
        
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let result: String
        if (filteredItems.count > indexPath.row) {
            result = filteredItems[indexPath.row]
        } else {
             result = self.searchBar!.text!.trimSpaces()
        }
        
        self.delegate.onResultPicked(result, senderTag: self.senderTag , resultId: nil)
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
