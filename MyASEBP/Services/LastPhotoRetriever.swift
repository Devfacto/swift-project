//
//  LastPhotoRetriever.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-20.
//  Copyright © 2016 devfacto. All rights reserved.
//

import Foundation

import UIKit
import Photos

struct LastPhotoRetriever {
    func queryLastPhoto(resizeTo size: CGSize?, queryCallback: (UIImage? -> Void)) {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
        //        fetchOptions.fetchLimit = 1 // This is available in iOS 9.
        
        let fetchResult = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: fetchOptions)
        
        if let asset = fetchResult.firstObject as? PHAsset {
            let manager = PHImageManager.defaultManager()
            
            // If you already know how you want to resize,
            // great, otherwise, use full-size.
            let targetSize = size == nil ? CGSize(width: asset.pixelWidth, height: asset.pixelHeight) : size!
            
            // I arbitrarily chose AspectFit here. AspectFill is
            // also available.
            manager.requestImageForAsset(asset,
                                         targetSize: targetSize,
                                         contentMode: .AspectFit,
                                         options: nil,
                                         resultHandler: { image, info in
                                            if (image?.size == targetSize)
                                            {
                                                queryCallback(image)
                                            }
            })
        }
        
    }
}