//
//  Float.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-07.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit
 

extension Double {
    
    
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
    
    func inCurrencyFormat() -> String {
        return String(format: "$%.02f", self)
    }
    
    
}

