//
//  NSDate.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-17.
//  Copyright © 2016 devfacto. All rights reserved.
//

import Foundation

class DateFormatter {
 
    private static var internalShortDateFormatter: NSDateFormatter?
    private static var internalFullDateFormatter: NSDateFormatter?
    private static var internalFullDateTimeFormatter: NSDateFormatter?
 

    static var shortDateFormatter: NSDateFormatter {
        if (internalShortDateFormatter == nil) {
            internalShortDateFormatter = NSDateFormatter()
            internalShortDateFormatter!.dateFormat = "MMM d, yyyy"
        }
        return internalShortDateFormatter!
    }

    static var fullDateFormatter: NSDateFormatter {
        if (internalFullDateFormatter == nil) {
            internalFullDateFormatter = NSDateFormatter()
            internalFullDateFormatter!.dateFormat = "MMMM d, yyyy"
        }
        return internalFullDateFormatter!
    }
    
    static var fullDateTimeFormatter: NSDateFormatter {
        if (internalFullDateTimeFormatter == nil) {
            internalFullDateTimeFormatter = NSDateFormatter()
            internalFullDateTimeFormatter!.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        }
        return internalFullDateTimeFormatter!
    }
    
}

extension NSDate {
    
    
    
    public func shortDateFormat() -> String
    {
        return DateFormatter.shortDateFormatter.stringFromDate(self)
    }
    
    public func fullDateFormat() -> String
    {
        return DateFormatter.fullDateFormatter.stringFromDate(self)
    }
    
    public func fullDateTimeFormat() -> String
    {
        return DateFormatter.fullDateTimeFormatter.stringFromDate(self)
    }
    
    
    public func addMonth(month: Int) -> NSDate
    {
        let components = NSDateComponents()
        components.month = month
        
        let result = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: self, options: .MatchStrictly )
    
        return result!
    }

}
