//
//  TableSectionHeader.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-11.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class TableSectionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var constraintErrorButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var addPhotoButton: UIButton!
 

}
