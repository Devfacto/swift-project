//
//  CALayer.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-09.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

extension CALayer {
    func borderUIColor() -> UIColor? {
        return borderColor != nil ? UIColor(CGColor: borderColor!) : nil
    }
    
    func setBorderUIColor(color: UIColor) {
        borderColor = color.CGColor
    }
    


}

