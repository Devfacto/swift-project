//
//  JSON.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-16.
//  Copyright © 2016 devfacto. All rights reserved.
//


import SwiftyJSON
import UIKit

extension JSON {
    
    public var date: NSDate? {
        get {
            switch self.type {
            case .String:
                return Formatter.jsonDateFormatter.dateFromString(self.object as! String)
            default:
                return nil
            }
        }
    }
    
    public var dateTime: NSDate? {
        get {
            switch self.type {
            case .String:
                if let result = Formatter.jsonDateTimeFormatter.dateFromString(self.object as! String)
                {
                    return result
                }
                if let result = Formatter.jsonDateTimeFormatter2.dateFromString(self.object as! String)
                {
                    return result
                }
                return nil
                
            default:
                return nil
            }
        }
    }
    
}

class Formatter {
    
    private static var internalJsonDateFormatter: NSDateFormatter?
    private static var internalJsonDateTimeFormatter: NSDateFormatter?
    private static var internalJsonDateTimeFormatter2: NSDateFormatter?
    
    static var jsonDateFormatter: NSDateFormatter {
        if (internalJsonDateFormatter == nil) {
            internalJsonDateFormatter = NSDateFormatter()
            internalJsonDateFormatter!.dateFormat = "MMM dd, yyyy"
        }
        return internalJsonDateFormatter!
    }
    
    static var jsonDateTimeFormatter: NSDateFormatter {
        if (internalJsonDateTimeFormatter == nil) {
            internalJsonDateTimeFormatter = NSDateFormatter()
            internalJsonDateTimeFormatter!.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZ'"
            
//            print(internalJsonDateTimeFormatter?.stringFromDate(NSDate()))
        }
        return internalJsonDateTimeFormatter!
    }
    
    static var jsonDateTimeFormatter2: NSDateFormatter {
        if (internalJsonDateTimeFormatter2 == nil) {
            internalJsonDateTimeFormatter2 = NSDateFormatter()
            internalJsonDateTimeFormatter2!.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSZ'"
            
            //            print(internalJsonDateTimeFormatter?.stringFromDate(NSDate()))
        }
        return internalJsonDateTimeFormatter2!
    }
    
}