//
//  String.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-16.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

class FileUtil {
    class func readStringFromFile(fileName: String, ext: String) -> String?
    {
        
        
        guard let path = NSBundle.init(forClass: BenefitsTests.self).pathForResource(fileName, ofType: ext) else {
            return nil
        }
        
        do {
            let content = try String(contentsOfFile:path, encoding: NSUTF8StringEncoding)
            return content
        } catch _ as NSError {
            return nil
        }
    }
}
