//
//  String.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-07-08.
//  Copyright © 2016 devfacto. All rights reserved.
//

import Foundation

extension String
{
    func trimSpaces() -> String
    {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
    func urlEncoded() -> String
    {
        let s = NSCharacterSet.URLQueryAllowedCharacterSet().mutableCopy()
        s.removeCharactersInString("+&")
        
        return self.stringByAddingPercentEncodingWithAllowedCharacters(s as! NSCharacterSet)!
        
    }
}