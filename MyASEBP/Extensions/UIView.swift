//
//  UIView.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-06-28.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

extension UIView
{

    var parentTableViewCell: UITableViewCell? {
        var view: UIView? = self
        while view != nil {
            view = view!.superview
            if let cell = view as? UITableViewCell {
                return cell
            }
        }
        return nil
    }
 
}