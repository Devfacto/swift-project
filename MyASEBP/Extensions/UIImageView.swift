//
//  URL.swift
//  My ASEBP
//
//  Created by devfacto on 2016-05-11.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

extension UIImageView {

    
    func getImageFromUrlString(urlString:String)
    {
        if let URL = NSURL(string: urlString)
        {
            getDataFromUrl(URL) { (data, response, error) in
                if let error = error
                {
                    NSLog("getting \(URL.absoluteString) failed with error \(error.localizedDescription)")
                }
                
                if let data = data
                {
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.image = UIImage(data: data)
                    }
                    
                }
                
            }
        }
    }
    
    private func getDataFromUrl(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data, response: response, error: error)
            }.resume()
    }

}
