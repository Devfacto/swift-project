//
//  UIColor.swift
//  MyASEBP-iOS
//
//  Created by devfacto on 2016-05-26.
//  Copyright © 2016 devfacto. All rights reserved.
//

import UIKit

extension UIColor
{
    class func navigationBarDarkBlueColor() -> UIColor
    {
        return UIColor(red: 29/255.0,
                       green: 67/255.0,
                       blue: 108/255.0,
                       alpha: 1)
    }
    
    class func placeholderGrayColor() -> UIColor
    {
        return UIColor(red: 199/255.0,
            green: 199/255.0,
            blue: 205/255.0,
            alpha: 1.0)
    }
    
    class func greenProgressViewColor() -> UIColor
    {
        return UIColor(red: 13/255.0,
                       green: 189/255.0,
                       blue: 198/255.0,
                       alpha: 1)
    }
    
    class func lightGreyProgressViewBackColor() -> UIColor
    {
        return UIColor(red: 240/255.0,
                       green: 240/255.0,
                       blue: 240/255.0,
                       alpha: 1)
    }
    
    class func purpleProgressViewColor() -> UIColor
    {
        return UIColor(red: 182/255.0,
                       green: 129/255.0,
                       blue: 231/255.0,
                       alpha: 1)
    }
    
    class func statusColorForNegative() -> UIColor
    {
        return UIColor(red: 251/255.0
            , green: 73/255.0
            , blue: 55/255.0
            , alpha: 1.0)
    }
    
    
    class func statusColorForPositive() -> UIColor
    {
        return UIColor(red: 54/255.0
            , green: 181/255.0
            , blue: 76/255.0
            , alpha: 1.0)
    }
    
    
    class func statusColorForNormal() -> UIColor
    {
        return UIColor.darkGrayColor()
    }
}

